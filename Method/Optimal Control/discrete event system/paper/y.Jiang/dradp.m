% procedure of TIE
close all
clear all
clc

digits(20);
syms s

A = 53.2;
H = 3.2;
kp1 = 17.9;
kp2 = 0.04;
ke1 = 65.6;
ke2 = 316;
hp = 2.8;
qa = 17;
qc = 7.392;
qT = 9.3;
ga = 0.0234;
gcp1 = 0.417;
gcp2 = 0.0034;
Xa1 = 0.1549;
Xa2 = (gcp1 - ga)/(ga - gcp2)*Xa1;
Lcu = 0.421;

kq = qT/(qa - qc);

Mp1 = 16.8;
Mp2 = 824.266;
Me1 = 4.56;
Me2 = 0.104;

tao = 3;
u1eq = 0.5;
u2eq = 170/3;
g = 9.8;
k0 = 0.3;
k1 = 4.6;

A1_linear = [k1/2/A*u1eq*sqrt(2*g)/sqrt(hp),0;0,-1/tao];
B1_linear = [-k1/A*sqrt(2*g*hp),0;0,k0/tao];
C1_linear = eye(2);
D1_linear = zeros(2);

G = ss(A1_linear,B1_linear,C1_linear,D1_linear);
Td = 3;

Gd = c2d(G,Td);

Gtf = tf(Gd);

[AA,BB]=tfdata(Gtf);

xa1 = BB{1}(2);
xa2 = BB{4}(2);
xb1 = AA{1}(2);
xb2 = AA{4}(2);

T1 = [-0.3,0;0,-0.4];
T2 = [0.09,0;0,0.04];

Kp1 = inv([xb1,0;0,xb2])*(-[xa1,0;0,xa2]-T2);
Ki1 = inv([xb1,0;0,xb2])*(eye(2)+T1+T2);

To = 30; 
Ts = 0.001;
simtime = 1200;
step = simtime/Ts;
uppercontrolstep = simtime/To;
uppercontrolstepter = To/Ts;
lowercontrolstep = simtime/Td;
lowercontrolstepter = Td/Ts;
m = To/Td;

int_error = zeros(2,uppercontrolstep);
error = zeros(2,lowercontrolstep);
error2 = zeros(2,uppercontrolstep);

upper_count = 1;
lower_count = 1;

x = zeros(4,step);
dotx = zeros(4,step);
doty = zeros(2,step);
Lcg = zeros(1,step);
Ltg = zeros(1,step);
y = [hp,0;0,qa]*ones(2,step);
ystar = [2.1,0;0,qa]*ones(2,lowercontrolstep);
yset = [2.1,0;0,qa]*ones(2,step);
u = [u1eq,0;0,u2eq]*ones(2,step);
%u = [0.48,0;0,14]*ones(2,step);
r = zeros(2,step);

rcgmax = 17.5;
rcgmin = 17.2;
rtgmax = 0.9;
rtgmin = 0.6;
rstar = [(rcgmax+rcgmin)/2;(rtgmax+rtgmin)/2];
rset = [rstar(1),0;0,rstar(2)]*ones(2,step);

y1max = 3;
y1min = 1;
y2max = 30;
y2min = 10;
Y_bar = [(y1max-y1min)/2,0;0,(y2max-y2min)/2];

Ad = [1,Td/A;0,1-Td/tao];
Bd = zeros(2,2,uppercontrolstep);

Ac = zeros(4,4,uppercontrolstep);
Bc = zeros(4,2,uppercontrolstep);

barB1 = zeros(4,4,m);
barB = eye(4);

gad = ga*ones(1,step);

Lc = 1;
La = diag([1,0.1]);
Vc = diag([0.1,1,1,0.1,1,0.1,0.1,1]);
Va = diag([0.1,1,1,0.1,1,0.1,0.1,1]);

mua = 0.5;
muc = 0.5;
beta_c = 0.8;
beta_a = 0.8;

Q = eye(2);
S = 0.1*eye(2);

Wc = (randn(1,8,uppercontrolstep) - 0.5)*0.2;
Wa = (randn(2,8,uppercontrolstep) - 0.5)*0.2;

xi = zeros(8,uppercontrolstep);

critic_err = 10;
actor_err = 10;

ec = 0;
ea = zeros(2,1);

gama = 0.8;

hatV = zeros(1,uppercontrolstep);

for i = 2:step-1
    
    doty(1,i) = y(2,i)/A-k1/A*sqrt(2*g*y(1,i))*u(1,i);
    doty(2,i) = -1/tao*y(2,i)+k0/tao*u(2,i);
    
    y(1,i+1) = y(1,i) + doty(1,i)*Ts;
    y(2,i+1) = y(2,i) + doty(2,i)*Ts;
   
    
    dotx(1,i) = -(kp1 + kq*(y(2,i) - qc)/A/y(1,i))*x(1,i) + ke1*x(3,i) + y(2,i)*Xa1;
    dotx(2,i) = -(kp2 + kq*(y(2,i) - qc)/A/y(1,i))*x(2,i) + ke2*x(4,i) + y(2,i)*Xa2;
    dotx(3,i) = -(ke1 + (qc)/A/(H - y(1,i)))*x(3,i) + kp1*x(1,i);
    dotx(4,i) = -(ke2 + (qc)/A/(H - y(1,i)))*x(4,i) + kp2*x(2,i);
    
    x(1,i+1) = x(1,i) + dotx(1,i)*Ts;
    x(2,i+1) = x(2,i) + dotx(2,i)*Ts;
    x(3,i+1) = x(3,i) + dotx(3,i)*Ts;
    x(4,i+1) = x(4,i) + dotx(4,i)*Ts;
    
    Lcg(i) = (x(3,i)*gcp1 + x(4,i)*gcp2)/(x(3,i) + x(4,i))*Lcu*100;
    Ltg(i) = (x(1,i)*gcp1 + x(2,i)*gcp2)/(x(1,i) + x(2,i))*Lcu*100;
    
    r(:,i) = [Lcg(i);Ltg(i)];
    
    gad(i) = ga;    
    
    if (mod(i,lowercontrolstepter) == 0)
        
        lower_count = lower_count + 1;
        ystar(:,lower_count) = ystar(:,upper_count);
        error(:,lower_count) = ystar(:,lower_count) - y(:,i);
            
        u(:,i+1) = u(:,i) + Kp1*(error(:,lower_count) - ...
            error(:,lower_count - 1)) + Ki1*error(:,lower_count); 
        
        int_error(:,lower_count) = int_error(:,lower_count - 1) + ...
            error(:,lower_count);
        
        Bd(:,:,lower_count) = [-k1*Td/A*sqrt(2*g*y(1,i)),0;0,k0*Td/tao];
        Ac(:,:,lower_count) = [Ad - Bd(:,:,lower_count)*Kp1,Bd(:,:,lower_count)*Ki1;...
            -eye(2),eye(2)]; 
        Bc(:,:,lower_count) = [Bd(:,:,lower_count)*Kp1;eye(2)];
        
    else
        u(:,i+1) = u(:,i);
    end
    
    if (mod(i,uppercontrolstepter) == 0)
        
        upper_count = upper_count + 1;     
        error2(:,upper_count) = rstar - r(:,i);
        
        barB = zeros(4,2);
        barB1(:,:,1) = eye(4);
        
        for j = 2:m
            
            barB1(:,:,j) = barB1(:,:,j-1)*Ac(:,:,lower_count+1-j);
            
        end
        
        for j = 1:m
            
            barB = barB + barB1(:,:,j)*Bc(:,:,lower_count-j);
            
        end
        
        barbarB = [zeros(2,2);barB];
        
        GT = [barbarB;zeros(2,2)];
        
        xi(:,upper_count) = [r(:,i);y(:,i);int_error(:,lower_count);rstar;];
        
        critic_err = 10;
        actor_err = 10;
        newEc = 10;
        newEa = 10;
        
        if (i >= 2*uppercontrolstepter)
            
            while(abs(critic_err) > 0.1)
               
                hatV(upper_count) = Wc(:,:,upper_count)*tanh(Vc*xi(:,upper_count));
                hatV(upper_count-1) = Wc(:,:,upper_count)*tanh(Vc*xi(:,upper_count-1));
                
                ec = Lc*(rstar - r(:,i-uppercontrolstepter))'*Q...
                    *(rstar - r(:,i-uppercontrolstepter))+...
                    vpa(int(atanh((2*s-y1max-y1min)/(y1max-y1min))*...
                    Y_bar(1,1)*S(1,1),s,(y1max+y1min)/2,xi(3,upper_count-1))) + ...
                    vpa(int(atanh((2*s-y2max-y2min)/(y2max-y2min))*...
                    Y_bar(2,2)*S(2,2),s,(y2max+y2min)/2,xi(4,upper_count-1)))+...
                    gama*hatV(upper_count)-hatV(upper_count-1);
                
                Ec = 0.5*ec'*ec;
                
                Jc = gama*tanh(Vc*xi(:,upper_count)) - tanh(Vc*xi(:,upper_count-1));                
                deltaWc = Jc*ec'/(Jc'*Jc+muc);
                Wc(:,:,upper_count) = Wc(:,:,upper_count) - deltaWc';
                
                critic_err = hatV(upper_count) - Wc(:,:,upper_count)*tanh(Vc*xi(:,upper_count));
                
                hatV(upper_count) = Wc(:,:,upper_count)*tanh(Vc*xi(:,upper_count));
                hatV(upper_count-1) = Wc(:,:,upper_count)*tanh(Vc*xi(:,upper_count-1));
                
                ec = Lc*(rstar - r(:,i-uppercontrolstepter))'*Q...
                    *(rstar - r(:,i-uppercontrolstepter))+...
                    vpa(int(atanh((2*s-y1max-y1min)/(y1max-y1min))*...
                    Y_bar(1,1)*S(1,1),s,(y1max+y1min)/2,xi(3,upper_count-1))) + ...
                    vpa(int(atanh((2*s-y2max-y2min)/(y2max-y2min))*...
                    Y_bar(2,2)*S(2,2),s,(y2max+y2min)/2,xi(4,upper_count-1)))+...
                    gama*hatV(upper_count)-hatV(upper_count-1);
                
                newEc = 0.5*ec'*ec;
                
                if newEc < Ec
                    muc = muc*beta_c;
                else
                    muc = muc/beta_c;
                end
                
            end
            
            while(abs(actor_err) > 0.1)
                
                derivation2 = inv(Lc)*Wc(:,:,upper_count)*Vc*...
                    (eye(8)-diag(tanh(Va*xi(:,upper_count-1)).*tanh(Va*xi(:,upper_count-1))));
                
                %derivation3 = weight_hidden_output2_m(:,7:8,k)';
                
                y_star = Y_bar*tanh(-gama/2*inv(Y_bar*S)*GT'*derivation2') +...
                    [(y1max+y1min)/2;(y2max+y2min)/2];
                
                ea = Wa(:,:,upper_count)*tanh(Va*xi(:,upper_count-1)) - La*y_star;
                
                Ea = 0.5*ea'*ea;
                
                Ja = tanh(Va*xi(:,upper_count-1));                
                deltaWa = Ja*ea'/(Ja'*Ja+mua);
                Wa(:,:,upper_count) = Wa(:,:,upper_count) - deltaWa';
                
                actor_err = norm(deltaWa'*tanh(Va*xi(:,upper_count-1)));
                
                ea = Wa(:,:,upper_count)*tanh(Va*xi(:,upper_count-1)) - La*y_star;
                
                %y_star
                %Wa(:,:,upper_count)*tanh(Va*xi(:,upper_count-1))
                %inv(La)*Wa(:,:,upper_count)*tanh(Va*xi(:,upper_count))
                
                newEa = 0.5*ea'*ea;
                
                if newEa < Ea
                    mua = mua*beta_a;
                else
                    mua = mua/beta_a;
                end
                
            end
            
            ystar(:,upper_count) = inv(La)*Wa(:,:,upper_count)*tanh(Va*xi(:,upper_count));
            ystar(:,upper_count)
            
            if upper_count < uppercontrolstep
                Wa(:,:,upper_count+1) = Wa(:,:,upper_count);
                Wc(:,:,upper_count+1) = Wc(:,:,upper_count);
            end
            

        end
        
    end
    
    if lower_count > 0
        yset(:,i) = ystar(:,upper_count);
    end
 
end

i = i + 1;
Lcg(i) = (x(3,i)*gcp1 + x(4,i)*gcp2)/(x(3,i) + x(4,i))*Lcu*100;
Ltg(i) = (x(1,i)*gcp1 + x(2,i)*gcp2)/(x(1,i) + x(2,i))*Lcu*100;
r(:,i) = [Lcg(i);Ltg(i)];

figure;subplot(2,1,1);plot((1:step)*Ts,Lcg,(1:step)*Ts,rset(1,:),...
    (1:step)*Ts,rcgmax*ones(1,step),(1:step)*Ts,rcgmin*ones(1,step));
xlabel('time(minute)');ylabel('ore grade');legend('$L_{cg}$',...
    '$L^*_{cg}$','$L_{cg\max}$','$L_{cg\min}$');
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
%axis([0 simtime rset(1)-0.1 rset(1)+0.3]);
axis([0 simtime rcgmin-0.15 rcgmax+0.15]);

subplot(2,1,2);plot((1:step)*Ts,Ltg,(1:step)*Ts,rset(2,:),...
    (1:step)*Ts,rtgmax*ones(1,step),(1:step)*Ts,rtgmin*ones(1,step));
xlabel('time(minute)');ylabel('ore grade');legend('$L_{tg}$',...
    '$L^*_{tg}$','$L_{tg\max}$','$L_{tg\min}$');
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(findall(gcf,'type','line'),'linewidth',2);
axis([0 simtime rtgmin-0.05 rtgmax+0.15]);

%figure;plot((1:step)*Ts,x);

figure;subplot(2,1,1);plot((1:step)*Ts,y(2,:),(1:step)*Ts,yset(2,:),...
    (1:step)*Ts,y2max*ones(1,step),(1:step)*Ts,y2min*ones(1,step));
xlabel('time(minute)');ylabel('feed flow');legend('$q_{a}$','$q^*_{a}$','$q_{a\max}$','$q_{a\min}$');
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(findall(gcf,'type','line'),'linewidth',2);
axis([0 simtime y2min*0.9 y2max*1.1]);

subplot(2,1,2);plot((1:step)*Ts,y(1,:),(1:step)*Ts,yset(1,:),...
    (1:step)*Ts,y1max*ones(1,step),(1:step)*Ts,y1min*ones(1,step));
xlabel('time(minute)');ylabel('pulp level');legend('$h_{p}$','$h^*_{p}$','$h_{p\max}$','$h_{p\min}$');
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(findall(gcf,'type','line'),'linewidth',2);
axis([0 simtime y1min*0.9 y1max*1.1]);

figure;subplot(2,1,1);plot((1:step)*Ts,u(1,:));
xlabel('time(minute)');ylabel('openness');legend('$u_1$');
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(findall(gcf,'type','line'),'linewidth',2);

subplot(2,1,2);plot((1:step)*Ts,u(2,:));
xlabel('time(minute)');ylabel('speed');legend('$u_2$');
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(findall(gcf,'type','line'),'linewidth',2);

wa1 = [];
wa2 = [];
wa3 = [];
wa4 = [];
wa5 = [];
wa6 = [];
wa7 = [];
wa8 = [];
wa9 = [];
wa10 = [];
wa11 = [];
wa12 = [];
wa13 = [];
wa14 = [];
wa15 = [];
wa16 = [];

wc1 = [];
wc2 = [];
wc3 = [];
wc4 = []; 
wc5 = [];
wc6 = [];
wc7 = [];
wc8 = [];

for i = 1:uppercontrolstep
   
    wa1 = [wa1 Wa(1,1,i)];
    wa2 = [wa2 Wa(1,2,i)];
    wa3 = [wa3 Wa(1,3,i)];
    wa4 = [wa4 Wa(1,4,i)];
    wa5 = [wa5 Wa(1,5,i)];
    wa6 = [wa6 Wa(1,6,i)];
    wa7 = [wa7 Wa(1,7,i)];
    wa8 = [wa8 Wa(1,8,i)];
    wa9 = [wa9 Wa(2,1,i)];
    wa10 = [wa10 Wa(2,2,i)];
    wa11 = [wa11 Wa(2,3,i)];
    wa12 = [wa12 Wa(2,4,i)];
    wa13 = [wa13 Wa(2,5,i)];
    wa14 = [wa14 Wa(2,6,i)];
    wa15 = [wa15 Wa(2,7,i)];
    wa16 = [wa16 Wa(2,8,i)];
    
    wc1 = [wc1 Wc(1,1,i)];
    wc2 = [wc2 Wc(1,2,i)];
    wc3 = [wc3 Wc(1,3,i)];
    wc4 = [wc4 Wc(1,4,i)];
    wc5 = [wc5 Wc(1,5,i)];
    wc6 = [wc6 Wc(1,6,i)];
    wc7 = [wc7 Wc(1,7,i)];
    wc8 = [wc8 Wc(1,8,i)];
    
end

figure;plot(1:To:To*uppercontrolstep,wc1,1:To:To*uppercontrolstep,wc2,...
    1:To:To*uppercontrolstep,wc3,1:To:To*uppercontrolstep,wc4,...
    1:To:To*uppercontrolstep,wc5,1:To:To*uppercontrolstep,wc6,...
    1:To:To*uppercontrolstep,wc7,1:To:To*uppercontrolstep,wc8);
%xlabel('control step');
xlabel('time(minute)');
legend('$w^{(1)}_c$','$w^{(2)}_c$','$w^{(3)}_c$','$w^{(4)}_c$',...
    '$w^{(5)}_c$','$w^{(6)}_c$','$w^{(7)}_c$','$w^{(8)}_c$');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex')

figure;pwa=plot(1:To:To*uppercontrolstep,wa1,1:To:To*uppercontrolstep,wa2,...
    1:To:To*uppercontrolstep,wa3,1:To:To*uppercontrolstep,wa4,...
    1:To:To*uppercontrolstep,wa5,1:To:To*uppercontrolstep,wa6,...
    1:To:To*uppercontrolstep,wa7,1:To:To*uppercontrolstep,wa8,...
    1:To:To*uppercontrolstep,wa9,1:To:To*uppercontrolstep,wa10,...
    1:To:To*uppercontrolstep,wa11,1:To:To*uppercontrolstep,wa12,...
    1:To:To*uppercontrolstep,wa13,1:To:To*uppercontrolstep,wa14,...
    1:To:To*uppercontrolstep,wa15,1:To:To*uppercontrolstep,wa16);
xlabel('control step');
xlabel('time(minute)');
[legh,objh,outh,outm]=legend(pwa(9:16),'$w^{(21)}_a$','$w^{(22)}_a$','$w^{(23)}_a$','$w^{(24)}_a$',...
    '$w^{(25)}_a$','$w^{(26)}_a$','$w^{(27)}_a$','$w^{(28)}_a$');
set(legh,'position',[0.5,0.2,0.2,0.8]);
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex')
legh2=copyobj(legh,gcf);
[legh2,objh2]=legend(pwa(1:8),'$w^{(11)}_a$','$w^{(12)}_a$','$w^{(13)}_a$','$w^{(14)}_a$',...
    '$w^{(15)}_a$','$w^{(16)}_a$','$w^{(17)}_a$','$w^{(18)}_a$');
set(legh2,'position',[0.7,0.2,0.2,0.8]);
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex')

avr1 = mean(Lcg(7*uppercontrolstepter:uppercontrolstepter:step));

avr2 = mean(Ltg(7*uppercontrolstepter:uppercontrolstepter:step));

IEAr1 = sum(abs(error2(1,7:uppercontrolstep)));

IEAr2 = sum(abs(error2(2,7:uppercontrolstep)));

MSEr1 = sqrt(error2(1,7:uppercontrolstep)*error2(1,7:uppercontrolstep)'/...
    length(7:uppercontrolstep));

MSEr2 = sqrt(error2(2,7:uppercontrolstep)*error2(2,7:uppercontrolstep)'/...
    length(7:uppercontrolstep));

maxr1 = max(Lcg(7*uppercontrolstepter:uppercontrolstepter:step));
minr1 = min(Lcg(7*uppercontrolstepter:uppercontrolstepter:step));

maxerrorr1 = max(abs(error2(1,7:uppercontrolstep)));
maxerrorr2 = max(abs(error2(2,7:uppercontrolstep)));

maxerrory1 = max(abs(error(1,7:lowercontrolstep)));
maxerrory2 = max(abs(error(2,7:lowercontrolstep)));

IEAy1 = sum(abs(error(1,7:lowercontrolstep)));

IEAy2 = sum(abs(error(2,7:lowercontrolstep)));

MSEy1 = sqrt(error(1,7:lowercontrolstep)*error(1,7:lowercontrolstep)'/...
    length(7:lowercontrolstep));

MSEy2 = sqrt(error(2,7:lowercontrolstep)*error(2,7:lowercontrolstep)'/...
    length(7:lowercontrolstep));

avu1 = mean(u(1,7*lowercontrolstepter:lowercontrolstepter:step));

avu2 = mean(u(2,7*lowercontrolstepter:lowercontrolstepter:step));

flushu1 = mean(abs(u(1,7*lowercontrolstepter:lowercontrolstepter:step)-...
    u(1,6*lowercontrolstepter:lowercontrolstepter:step-1)));

flushu2 = mean(abs(u(2,7*lowercontrolstepter:lowercontrolstepter:step)-...
    u(2,6*lowercontrolstepter:lowercontrolstepter:step-1)));



























