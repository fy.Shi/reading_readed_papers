function odestart
% F-16 short period dynamics
clear all;close all;clc;
global P;
global u;
%system matrices
A =[-1.01887    0.90506   -0.00215
     0.82225   -1.07741   -0.17555    
      0         0         -20.2  ];
B=[0 0 20.2]';

figure(1);hold on;

%initial conditions
x0=[0.1,0.2,0.1,0];
P=[0 0 0;0 0 0; 0 0 0]; % critic parameters
uu=[]; % saving the control signal for plot

%parameters of the critic rearanged as it would be returned by the least
%squares
WW=[P(1,1); 2*P(1,2); 2*P(1,3); P(2,2); 2*P(2,3); P(3,3)];
WWP=[WW; 0];

Fsamples=60; %length of the simulation in samples 
T=.05; % sample time

%parameters for the batch least squares
j=0;
Xpi=[];

E=[eig(A-B*B'*P)]; % saves the poles of the closed loop system over the duration of the simulation
upd=[]; % stores information relative to updates of the critic parameters
ch=0;

for k=1:Fsamples,
    j=j+1;
    if k==21,
            %A(1,1)=-0.69; 
            % this can be uncommented to simulate a change
            %in the system dynamics
    end
    X(j,:)=[x0(1)^2 x0(1)*x0(2) x0(1)*x0(3) x0(2)^2 x0(2)*x0(3) x0(3)^2];
    before_cost=[x0(1) x0(2) x0(3)]*P*[x0(1) x0(2) x0(3)]';
    tspan=[0 T];
    [t,x]= ode45(@odefile,tspan,x0);
    x1=x(length(x),1);
    x2=x(length(x),2);
    x3=x(length(x),3);
    after_cost=x(length(x),4)+[x1 x2 x3]*P*[x1;x2;x3];
    Xpi(j,:)=X(j,:)-[x1^2 x1*x2 x1*x3 x2^2 x2*x3 x3^2];
    figure(1); 
    plot(t+T*(k-1),x(:,1),':b',t+T*(k-1),x(:,2),'r',t+T*(k-1),x(:,3),'-g');
    Y(j,:)=x(length(x),4);
    x0=[x(length(t),1) x(length(t),2) x(length(t),3) 0];
    plot(t(length(t))+T*(k-1),x0(1:3),'o');
    
    uu=[uu u];
    
    if (abs(after_cost-before_cost)>0.00001)&&(ch==0)&&(mod(j,6)~=1),
        j=0;
        ch=ch+1;
    else
        if abs(after_cost-before_cost)>0.00001,
        ch=ch+1;
        end
    end
    % the batch least squares is made on 6 values
    if mod(j,6)==0,
        
        if (abs(after_cost-before_cost)>0.00001)&&(ch==6),
        weights=Xpi\Y; %calculation of the weights
        upd=[upd 1];
        else 
        %there is no reason to update
        upd=[upd 0];       
        end
        WWP=[WWP [weights; k*T]];
        WW=[WW weights];
         %calculating the matrix P
        P=[weights(1) weights(2)/2 weights(3)/2 ; weights(2)/2 weights(4) weights(5)/2; weights(3)/2 weights(5)/2 weights(6)];
        j=0;
        ch=0;
        E=[E eig(A-B*B'*P)];
    end
end
figure(1); title('System states'); xlabel('Time(s)');

%printing for comparison
sol=care(A,B,eye(3),1)
P=[weights(1) weights(2)/2 weights(3)/2 ; weights(2)/2 weights(4) weights(5)/2; weights(3)/2 weights(5)/2 weights(6)]

WW=[WW [sol(1,1); 2*sol(1,2); 2*sol(1,3); sol(2,2); 2*sol(2,3); sol(3,3)]];

figure; plot([0:T:T*(length(uu)-1)],uu);
title('Control signal'); xlabel('Time(s)');

%plotting the poles of the closed loop system
figure;
for jj=1:Fsamples/6,
    plot(6*T*(jj-1),real(E(:,jj)),'.');
    hold on;
end
title('Poles of the closed loop system'); xlabel('Time(s)')

figure;
hold on;
plot(WWP(7,:)',WWP(1:6,:)','.-');
title('P matrix parameters'); xlabel('Time(s)')

% figure;
% WWP=[WWP [[sol(1,1); 2*sol(1,2); 2*sol(1,3); sol(2,2); 2*sol(2,3); sol(3,3)]; T*(Fsamples-1)]];
% xy=size(WWP);
% plot(WWP(7,1:(xy(2)-1)),WWP(2:2:6,1:(xy(2)-1)),'.');
% hold on;
% plot(WWP(7,xy(2)),WWP(2:2:6,xy(2)),'*');
% title('P matrix parameters'); xlabel('Time(s)'); axis([0 3 -0.2 2.5]);

figure;
plot(upd,'*'); title('P parameters updates'); xlabel('Iteration number');axis([0 10 -0.1 1.1]);
%-------------------------------------------------------------------------------------
function xdot=odefile(t,x)
global P;
global u;
A =[-1.01887    0.90506   -0.00215
     0.82225   -1.07741   -0.17555    
      0         0         -20.2  ];

B=[0 0 20.2]';
Q=eye(3);R=1;
x1=x(1);
x2=x(2);
x3=x(3);

  %calculating the control signal
u=-inv(R)*B'*P*[x1;x2;x3];

  %updating the derivative of the state=[x(1:2) V]
xdot=[A*[x1;x2;x3]+B*u
      [x1;x2;x3]'*Q*[x1;x2;x3]+u'*R*u];
