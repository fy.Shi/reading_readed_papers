function odestart
clear all;close all;clc;

global P;
global Target;
global v;

figure;hold on;

%initializations

%iteration step
j=0;

%initial state
x0=[1 1 0];
x0s=x0;
% P gives the controller parameters
P=[0 0 3/2 0 0 -0.1 0 3];

Target=0;
vv=[];
C=[];

Fsamples=100;
nop=20;

%next WW gives the initial stabilizing controller
WW=zeros(length(P),1+Fsamples/nop);
WW(:,1)=P';

for k=1:Fsamples,
    j=j+1;
    % simulation of the system to get the measurements
    T=.1;tspan=[0 T];
    [t,x]= ode23(@odefile,tspan,x0);
    x1=x(length(x),(1:2));
    X(j,:)=[x0(1)^2 x0(1)*x0(2) x0(2)^2 x0(1)^4 (x0(1)^3)*x0(2) (x0(1)^2)*(x0(2)^2) x0(1)*(x0(2)^3) x0(2)^4]'- [x1(1)^2 x1(1)*x1(2) x1(2)^2 x1(1)^4 (x1(1)^3)*x1(2) (x1(1)^2)*(x1(2)^2) x1(1)*(x1(2)^3) x1(2)^4]';
    
    Target=x(length(x),3);
    Y(j,:)=Target;
    
    if mod(k,nop/8)==0,
        x0=[2*(rand(1,2)-1/2) 0];
    else
        x0=[x1 0];
    end

    plot(t+T*(k-1),x(:,1));
    vv=[vv v];

    if mod(k,nop)==0,
        weights=X\Y;
        %calculating the matrix P
        P=[weights(1) weights(2) weights(3) weights(4) weights(5) weights(6) weights(7) weights(8)];
        WW(:,k/nop+1)=[weights(1) weights(2) weights(3) weights(4) weights(5) weights(6) weights(7) weights(8)]';
        X=zeros(nop,8);
        Y=zeros(nop,1);
        j=0;
        x0=[0.5*(rand(1,2)-1/2) 0];
    end

end
P=[weights(1) weights(2) weights(3) weights(4) weights(5) weights(6) weights(7) weights(8)]

title('System states'); xlabel('Time (s)');

figure; plot([0:T:T*(length(vv)-1)],vv); title('Control signal'); xlabel('Time (s)')

figure; % in this figure we plot the neural network parameters at each iteration step in the policy iteration
WW % the matrix of parameteres is printed in the comand window
ss=size(WW);
plot((0:T*nop:T*Fsamples),WW(:,1:ss(2))','.'); title('NN parameters'); xlabel('Time (s)'); %hold on; plot(T*(Fsamples+1),WW(:,length(WW))','*'); title('NN parameters');

%-------------------------------------------------------------------------------------
function xdot=odefile(t,x);
global P;
global v;

Q=[1 0; 0 1];R=1;
x=[x(1) x(2)]';

%calculating the control signal
% P are the parameters of the critic
v=-1/2*inv(R)*sin(x(1))*P*[0; x(1); 2*x(2); 0 ; (x(1))^3; 2*((x(1))^2)*x(2); 3*x(1)*(x(2)^2); 4*(x(2))^3];

%xdot=[A*[x1;x2;x3;x4]+B*v %+F*deltaPd
xdot=[[-1 1; -1/2 -1/2]*x+[2*(x(2))^3; 1/2*x(2)*(1+2*(x(2))^2)*(sin(x(1)))^2+sin(x(1))*v];   x'*Q*x+2*(x(2))^4+v'*R*v];

%-----------------------------------
