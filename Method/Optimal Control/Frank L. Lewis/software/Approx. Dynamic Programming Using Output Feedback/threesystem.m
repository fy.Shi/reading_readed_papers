%Name: Kyriakos Vamvoudakis
%company: ARRI
%date: 2009
%All rights reserved
%x_dot=Ax+Bu
%y=Cx
%with:
%A =
%
%   1.8000   -1.0700    0.2100
%  1.0000         0         0
%      0    1.0000         0
%B =
%     1
%     0
%     0

%C =
%         0    1.0000    0.8000


clear all
close all
clc
% close all; clc; clear all;
% A=[0 1; -.72 1.7]; B=[1;1];
x0=[1 1 1  ]';
x(:,1)=x0;
% C=[1 1];
% minimum phase systems
%
% Zeros=[1.2];
% Poles=[0.5 0.6 0.1]; %define the pole and zeros of the system (non minimum
% phase)
% Zeros=[0.8];Poles=[0.5 0.6 1.2];% minimum phase
Zeros=[-0.8];
Poles=[0.5 0.6 0.7]; %unstable
[num,den]=zp2tf(Zeros,Poles,1);
[A,B,C,D]=tf2ss(num,den);
figure(3)
pzmap(A,B,C,0)
y(1)=C*x0;
N=850;
Q=1*eye(3); R=1;
Q_out=1;
P(:,:,1)=  [0 0 0 0 0 0 ;0 0 0 0 0 0; 0 0 0 0 0 0 ; 0 0 0 0 0 0;0 0 0 0 0 0;0 0 0 0 0 0];
iter=50;
for j=1:iter
    for k=1:N
        if k==1
            y(k)=C*x(:,k); 
            u(k)=-.2*inv(R+.2*P(1,1,j))*P(1,4,j)*y(k)+34*(sin(100*k)^2*cos(100*k)+sin(2*k)^2*cos(0.1*k)+sin(-1.2*k)^2*cos(0.5*k)+sin(k)^5+sin(1.12*k)^2+cos(2.4*k)*sin(2.4*k)^3);
            x(:,k+1)=A*x(:,k)+B*u(k);
            x_d(:,k)=[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
            d(k)=y(k)'*Q_out*y(k)+u(k)'*R*u(k)+.2*[u(k); 0;0;y(k); 0;0]'*P(:,:,j)*[u(k); 0;0;y(k); 0;0] ;
        else if k==2
                y(k)=C*x(:,k);
                u(k)=-.2*inv(R+.2*P(1,1,j))*(P(1,2,j)*u(k-1) +[P(1,4,j) P(1,5,j)]*[y(k) y(k-1)]')+10*(sin(100*k)^2*cos(100*k)+sin(2*k)^2*cos(0.1*k)+sin(-1.2*k)^2*cos(0.5*k)+sin(k)^5+sin(1.12*k)^2+cos(2.4*k)*sin(2.4*k)^3);
                x(:,k+1)=A*x(:,k)+B*u(k);
                x_d(:,k)=[u(k-1)^2 0 0 2*u(k-1)*y(k-1) 0 0 0 0 0 0 0 0 0 0 0 y(k-1)^2 0 0 0 0 0 ];
                d(k)=y(k)'*Q_out*y(k)+u(k)'*R*u(k)+.2*[u(k); u(k-1); 0;y(k); y(k-1) ;0]'*P(:,:,j)*[u(k) ;u(k-1);0;y(k); y(k-1);0] ;
                
            else if k==3
                y(k)=C*x(:,k);
                u(k)=-.2*inv(R+.2*P(1,1,j))*([P(1,2,j) P(1,3,j)]*[u(k-1) u(k-2)]'+[P(1,4,j) P(1,5,j) P(1,6,j)]*[y(k) y(k-1) y(k-2)]')+.5*(sin(100*k)^2*cos(100*k)+sin(2*k)^2*cos(0.1*k)+sin(-1.2*k)^2*cos(0.5*k)+sin(k)^5+sin(1.12*k)^2+cos(2.4*k)*sin(2.4*k)^3);
                x(:,k+1)=A*x(:,k)+B*u(k);
                x_d(:,k)=[u(k-1)^2 2*u(k-1)*u(k-2) 0 2*u(k-1)*y(k-1) 2*u(k-1)*y(k-2) 0 u(k-2)^2 0 2*u(k-2)*y(k-1) 2*u(k-2)*y(k-2) 0 0 0 0 0 y(k-1)^2 2*y(k-1)*y(k-2) 0 y(k-2)^2 0 0 ];
                d(k)=y(k)'*Q_out*y(k)+u(k)'*R*u(k)+.2*[u(k); u(k-1); u(k-2);y(k); y(k-1) ;y(k-2)]'*P(:,:,j)*[u(k); u(k-1); u(k-2);y(k); y(k-1) ;y(k-2)] ;
                else
                y(k)=C*x(:,k);
                u(k)=-.2*inv(R+.2*P(1,1,j))*([P(1,2,j) P(1,3,j)]*[u(k-1) u(k-2)]'+[P(1,4,j) P(1,5,j) P(1,6,j)]*[y(k) y(k-1) y(k-2)]')+.01*(sin(100*k)^2*cos(100*k)+sin(2*k)^2*cos(0.1*k)+sin(-1.2*k)^2*cos(0.5*k)+sin(k)^5+sin(1.12*k)^2+cos(2.4*k)*sin(2.4*k)^3);
                x(:,k+1)=A*x(:,k)+B*u(k);
                x_d(:,k)=[u(k-1)^2 2*u(k-1)*u(k-2) 2*u(k-1)*u(k-3) 2*u(k-1)*y(k-1) 2*u(k-1)*y(k-2) 2*u(k-1)*y(k-3) u(k-2)^2 2*u(k-2)*u(k-3) 2*u(k-2)*y(k-1) 2*u(k-2)*y(k-2) 2*u(k-2)*y(k-3) u(k-3)^2 2*u(k-3)*y(k-1) 2*u(k-3)*y(k-2) 2*u(k-3)*y(k-3) y(k-1)^2 2*y(k-1)*y(k-2) 2*y(k-1)*y(k-3) y(k-2)^2 2*y(k-2)*y(k-3) y(k-3)^2 ];    
                d(k)=y(k)'*Q_out*y(k)+u(k)'*R*u(k)+.2*[u(k); u(k-1); u(k-2);y(k); y(k-1) ;y(k-2)]'*P(:,:,j)*[u(k); u(k-1); u(k-2);y(k); y(k-1) ;y(k-2)] ;
                
                
                end
    end
end
    end
  P_new = pinv(x_d')*d';
  
  P(1,1,j+1)=P_new(1);
 P(1,2,j+1)=P_new(2); 
 P(1,3,j+1)=P_new(3);
 P(1,4,j+1)=P_new(4);
 P(1,5,j+1)=P_new(5);
 P(1,6,j+1)=P_new(6); 
 P(2,1,j+1)=P_new(2);
 P(2,2,j+1)=P_new(7);
 P(2,3,j+1)=P_new(8);
 P(2,4,j+1)=P_new(9);
 P(2,5,j+1)=P_new(10);
 P(2,6,j+1)=P_new(11);
 P(3,1,j+1)=P_new(3);
 P(3,2,j+1)=P_new(8);
 P(3,3,j+1)=P_new(12);
 P(3,4,j+1)=P_new(13);
 P(3,5,j+1)=P_new(14);
 P(3,6,j+1)=P_new(15);
 P(4,1,j+1)=P_new(4);
 P(4,2,j+1)=P_new(9);
 P(4,3,j+1)=P_new(13);
 P(4,4,j+1)=P_new(16);
 P(4,5,j+1)=P_new(17);
 P(4,6,j+1)=P_new(18);
 P(5,1,j+1)=P_new(5);
 P(5,2,j+1)=P_new(10);
 P(5,3,j+1)=P_new(14);
 P(5,4,j+1)=P_new(17);
 P(5,5,j+1)=P_new(19);
 P(5,6,j+1)=P_new(20);
 P(6,1,j+1)=P_new(6);
 P(6,2,j+1)=P_new(11);
 P(6,3,j+1)=P_new(15);
 P(6,4,j+1)=P_new(18);
 P(6,5,j+1)=P_new(20);
 P(6,6,j+1)=P_new(21);
  
  
end
    
figure (1);
for k=1:iter
    hold on;
    p11(k)=P(1,1,k);

    
end
plot(p11,'b');
for k=1:iter
%     for i=1:2
    hold on;
    p12(k)=P(1,2,k);

end
hold on;
plot(p12,'r');
for k=1:iter
%     for i=1:2
    hold on;
    p22(k)=P(1,3,k);

end
hold on;
plot(p22,'g');

for k=1:iter
%     for i=1:2
    hold on;
    p23(k)=P(1,4,k);

end
hold on;
plot(p23,'y');
for k=1:iter
%     for i=1:2
    hold on;
    p24(k)=P(1,5,k);

end
hold on;
plot(p24,'m');

for k=1:iter
%     for i=1:2
    hold on;
    p25(k)=P(1,6,k);

end
hold on;
plot(p25,'b');

figure (2);
 plot(x(1,:),'linewidth',1.2); hold on; plot(x(2,:),'r')
  Ric=[  

    0.5568   -0.0211    0.0371
   -0.0211    1.2293    0.7789
    0.0371    0.7789    0.6444

];
% Ric= dare(A,B,Q,R);
U=[B A*B (A^2)*B];
% U=B;
% 
% M=A*inv([C ;C*A]'*[C ;C*A])*[C ;C*A]';
V=[C*(A^2);C*A;C];
% V=C;
%  M_new=(A^3)*inv(V'*V)*V';
 M_new=(A^3)*pinv(V);
 T=[0 C*B C*A*B;  0 0 C*B; 0 0 0]; 
 Mu=U-M_new*T;
% % dcom=[M_new(1,1)^2+M_new(1,2)*M_new(2,1) M_new(1,1)*M_new(2,1)+M_new(1,2)*M_new(2,2); M_new(2,1)*M_new(1,1)+M_new(2,2)*M_new(2,1) M_new(2,1)*M_new(1,1)+M_new(2,2)^2];

% 
P_opt=[Mu M_new]'*Ric*[Mu M_new];%  pcom=inv(dcom)*P;

 
    