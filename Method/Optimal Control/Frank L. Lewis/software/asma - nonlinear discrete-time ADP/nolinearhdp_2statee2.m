
% 2 state  nolinear system , HJB (ADP)
% 14/07/06
%Prepared by Asma Al-Tamimi

function f=cost(wu)
global k
global Z
global Wv
global Q
global Numberofpoint

clc; clear all;close all;


Q=eye(2,2);     R=1;     alpha1=0.0001; 

Wv=[0;0;0;0;0;0;0;0;0;0;0;0;0;0;0];
x1set=401;
x2set=201;
x1start=-2;
x2start=-1;
Numberofpoint=x1set*x2set;
Wu=[0;0;0;0;0;0;0;0;0;0;0;0];
phix1=[0;0;0;0;0;0;0;0;0;0;0;0;0;0;0];
V(1:Numberofpoint,1)=0;
X(1:Numberofpoint,15)=0;
x11=[0;0];
x=[0;0];
Vx1=0;
n=2;% number of t
phix1dash=[ 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]; u=0;x1=0;x2=0;
sigmax=[0;0;0;0;0;0;0;0;0;0;0;0];
Z(Numberofpoint,1:n)=0;%training set
Calling=1:Numberofpoint;
% Generating the training set.

for trainingsetx1=1:x1set
    x1=x1start+(trainingsetx1-1)*0.01;

    for trainingsetx2=1:x2set

        x2=x2start+(trainingsetx2-1)*0.01;
        
        Z((trainingsetx1-1)*x2set+trainingsetx2,:)=[x1 x2];
    end
end



     for Policyiteration=1:100
         Policyiteration
         
        Calling= Calling(randperm(Numberofpoint));

     for Vtrainingdata=1:Numberofpoint  % weigth tuning for the value function (LS)

         x=Z(Calling(Vtrainingdata),:)';
         x=Z(Vtrainingdata,:)';
       
         x1=x(1);x2=x(2);

         sigmax= [x1;  x2; x1^3; x1^2*x2; x1*x2^2; x2^3; x1^5; x1^4*x2;x1^3*x2^2;x1^2*x2^3;x1*x2^4;x2^5];
         u=Wu'*sigmax;
        
         X1=[.2*x1*exp(x2^3); .3*x2^3]+[0;-.2]*u;
      
         phix1=[X1(1)^2; X1(1)*X1(2); X1(2)^2;  X1(1)^4; X1(1)^3*X1(2); X1(1)^2*X1(2)^2; X1(1)*X1(2)^3; X1(2)^4;
            X1(1)^6; X1(1)^5*X1(2);X1(1)^4*X1(2)^2;X1(1)^3*X1(2)^3;X1(1)^2*X1(2)^4;X1(1)*X1(2)^5;X1(2)*6];

         Vx1hat=Wv'*phix1;
       
         V( Vtrainingdata,:)=x'*Q*x+u'*u+ Vx1hat;
         X( Vtrainingdata,:)=[x1^2 x1*x2 x2^2  x1^4 x1^3*x2 x1^2*x2^2 x1*x2^3 x2^4 x1^6 x1^5*x2 x1^4*x2^2 x1^3*x2^3 x1^2*x2^4 x1*x2^5 x2^6];

     end

     Wv=lsqr(X,V,1e-15,1000000)
     %Wv=X\V
    % Wu=[0;0;0;0;0;0;0;0;0;0;0;0];
   
     for Uepochs=1:100
         Calling= Calling(randperm(Numberofpoint));
       alpha=.001*(100-Uepochs)/100;

         for Utraningdata=1:Numberofpoint

            x=Z(Calling(Utraningdata),:)';
       
             x1=x(1);x2=x(2);
             sigmax=[x1;  x2; x1^3; x1^2*x2; x1*x2^2; x2^3; x1^5; x1^4*x2;x1^3*x2^2;x1^2*x2^3;x1*x2^4;x2^5];

             u= Wu'*sigmax;


             X1=[.2*x1*exp(x2^3); .3*x2^3]+[0;-.2]*u;
             x1=X1(1);
             x2=X1(2);

             phix1dash=[2*x1             0
                 x2     		    x1
                 0      		    2*x2
                 4*x1^3           0
                 3*x1^2*x2        x1^3
                 2*x1*x2^2        2*x1^2*x2
                 x2^3       	    3*x1*x2^2
                 0                4*x2^3
                 6*x1^5			0
                 5*x1^4*x2		x1^5
                 4*x1^3*x2^2	    2*x1^4*x2
                 3*x1^2*x2^3	    3*x1^3*x2^2
                 2*x1*x2^4		4*x1^2*x2^3
                 x2^5             5*x1*x2^4
                 0				6*x2^5 ];

             Vx1=phix1dash'*Wv;
             x1=x(1);
             x2=x(2);
            

             Vw=(2*R*u+([0;-.2])'*Vx1);
            %alpha=alpha*(Numberofpoint-Utraningdata+1)/Numberofpoint;

             Wu=Wu-alpha*(sigmax*Vw);
         end
      
     end
Wu
% Wu=lsqnonlin(@cost1,Wu,[],[],optimset('LargeScale','off','LevenbergMarquardt','off'))
 end

