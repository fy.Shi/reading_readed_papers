clear all
close all
clc

step = 300;

A = [-1,2;2.2,1.7];
B = [2;1.6];
C = [1,2];
F = -1;

Q = 6;
R = 1;
gama = 0.8;

A2 = [A,zeros(2,1);
    zeros(1,2),F];
B2 = [B;0];
C2 = [C,0];
C1 = [C,-1];
Q1 = C1'*Q*C1;

dropoutmax = 1;

M_bar = [A,B];
M = [eye(2),M_bar,zeros(2,1);
    zeros(1,2),zeros(1,3),1];
M_star = M'*inv(M*M');

K = [0.2 -1.1 0.05];
%K = [0.3 1.3 0.75];
K_bar = K*M;

K_bar_star = [0.1898   -1.0085   -2.4086   -1.3348   -1.2340    0.0640];

P_bar = zeros(9,9,step);
P = zeros(3,3,step);
P_star = [133.3840 16.0531 31.1402;
        16.0531 25.1604 -10.8271;
        31.1402 -10.8271 18.4825;];
P_bar_star = M'*P_star*M;
Huz_star = gama*B2'*P_star*A2*M;
Huu_star = R + gama*B2'*P_star*B2;
H_star = [M'*Q1*M+gama*M'*A2'*P_star*A2*M,Huz_star';
    Huz_star,Huu_star;];

P_learn = zeros(3,3,step);

x = [5,0;0,-5]*ones(2,step);
u = zeros(1,step);
y = zeros(1,step);
r = zeros(1,step);
z = zeros(6,step);
dropout = ones(1,step); 
e = zeros(1,step);

r(1,1) = 5;

for i = 1:step-1
    
    r(1,i+1) = F*r(1,i);
    
end
for i = 1:step-50
    
    %e(i) = (sin(i)+sin(6*i)+sin(25*i)+sin(128*i)+sin(299*i))/20;
    e(i) = (rand(1)-0.5);
    
end

for i = 5:step
    
    %if ((sin(i)+cos(i)+cos(3*i)+sin(5*i)<=-0.2))&&(dropout(i-1) == 1)
    if (rand(1)<=0.3)&&(dropout(i-1) == 1)
        dropout(i) = 0;
    end
    
end

z(:,2) = [x(:,2)',0,0,0,r(2)]';
u(2) = K_bar*z(:,2) + e(2);
    
x(:,3) = A*x(:,2) + B*u(2);
y(2) = C*x(:,2);

xx = [];
uu = [];

kran = 0;
xxxx = [];
uuu = [];
uuu1 = [];
delta_norm = zeros(1,step);
Huz_learn = zeros(6,step);
Huu_learn = zeros(1,step);
delta_Huz_norm = zeros(1,step);
delta_Huu_norm = zeros(1,step);

delta_Huz_norm(1) = norm(Huz_star' - Huz_learn(:,1));
delta_Huu_norm(1) = norm(Huu_star - Huu_learn(:,1));
delta_norm(1) = norm(K_bar - K_bar_star);

delta_Huz_norm(2) = norm(Huz_star' - Huz_learn(:,2));
delta_Huu_norm(2) = norm(Huu_star - Huu_learn(:,2));
delta_norm(2) = norm(K_bar - K_bar_star);

for i = 3:step
    
    if dropout(i) == 1
        z(:,i) = [x(:,i)',0,0,0,r(i)]';
    end
    if dropout(i) == 0
        z(:,i) = [0,0,x(:,i-1)',u(i-1),r(i)]';
    end
    
    xx1 = kron([z(:,i-1)',u(i-1)],[z(:,i-1)',u(i-1)]) - ...
        gama*kron([z(:,i)',K_bar*z(:,i)],[z(:,i)',K_bar*z(:,i)]);
    %xx1 = kron([z(:,i-1)',u(i-1)],[z(:,i-1)',u(i-1)]);
    xx2 = -kron(z(3:5,i-1)',z(3:5,i-1)');
    xx3 = 2*kron(z(3:5,i-1)',r(i-1));
    
    %xx = [xx;xx1,xx2,xx3];    
    xx = [xx;xx1,xx2,xx3];
    
    uu1 = u(i-1)'*R*u(i-1);
    uu2 = z(1:2,i-1)'*C'*Q*C*z(1:2,i-1);
    uu3 = r(i-1)'*Q*r(i-1);
    uu4 = r(i-1)'*Q*C*z(1:2,i-1);
    
    uu = [uu;uu1+uu2+uu3-2*uu4];
    %uu = [uu;uu2+uu3-2*uu4];
    %uuu1 = [uuu1;kron(z(:,i-1)',z(:,i-1)')];
    uuu1 = [];
    rank(xx'*xx);
    %dropout(i);
    delta_norm(i) = norm(K_bar - K_bar_star);
%     if(i>120)
%         xxxx = xx(i-105:i-5,:);
%         uuu = uu(i-105:i-5);
%     end
    delta_Huz_norm(i) = norm(Huz_star' - Huz_learn(:,i-1));
    delta_Huu_norm(i) = norm(Huu_star - Huu_learn(:,i-1));
    
    if (rank(xx'*xx) == 31)
    %if (rank(xxxx'*xxxx) == 31)
        kran = kran + 1;
        xxx = [xx(:,1),2*xx(:,2),2*xx(:,6),2*xx(:,7),xx(:,9),...
            2*xx(:,13:14),xx(:,17),2*xx(:,18:19),2*xx(:,20),2*xx(:,21),...
            xx(:,25),2*xx(:,26),2*xx(:,27),2*xx(:,28),xx(:,33),...
            2*xx(:,34:35),xx(:,41),2*xx(:,42),xx(:,49),xx(:,50),...
            2*xx(:,51:52),xx(:,54),2*xx(:,55),xx(:,58:61)];
        %xxxx = xxx(i-105:i-5,:);
        %uuu = uu(i-105:i-5);
        %if norm(xxx'*xxx) > 0.5
        %for j = 1:100            
            KKK = K_bar'*R*K_bar;
            %vecH_bar = inv(xxx'*xxx)*xxx'*[uu+uuu1*KKK(:)];
            vecH_bar = inv(xxx'*xxx)*xxx'*uu;
            %norm(xxx'*xxx)
        %end
        %vecH_bar = inv(xxxx'*xxxx)*xxxx'*uuu;
        %P_learn(:,:,i) = [vecP(1:3),[vecP(2);vecP(4:5)],...
        %    [vecP(3);vecP(6);vecP(6)]];
            Huz = [vecH_bar(4),vecH_bar(7),vecH_bar(12),vecH_bar(16),...
                vecH_bar(19),vecH_bar(21)]
            Huz_learn(:,i) = Huz';
            Huu = vecH_bar(22)
            Huu_learn(:,i) = Huu;            
            K_bar = -inv(Huu)*Huz
        %end
        %K_bar = K_bar_star
        %end
        %K = -inv(R+gama*B2'*P_learn(:,:,i)*B2)*...
        %    gama*B2'*P_learn(:,:,i)*A2;
        xx = [];
        uu = [];
        uuu1 = [];
    else
        Huz_learn(:,i) = Huz_learn(:,i-1);
        Huu_learn(:,i) = Huu_learn(:,i-1);
    end
    
    u(i) = K_bar*z(:,i) + e(i);
    
    x(:,i+1) = A*x(:,i) + B*u(i);
    y(i) = C*x(:,i);
    
end

Huz_learn(:,step) = Huz_learn(:,step-1);
Huu_learn(:,step) = Huu_learn(:,step-1);

K_bar_star

%f1
figure;subplot(2,1,1),plot(1:step,y(1:step),1:step,r(1:step));
xlabel('step');legend('$y$','$r$');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(get(gca,'YLabel'),'Interpreter','latex');



subplot(2,1,2),plot(1:step,u(1:step));
xlabel('step');legend('$u$');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(get(gca,'YLabel'),'Interpreter','latex');
axis([1 step -25 25]);
axes('Position',[0.75 0 0.1 1]);
plot(1:step,y(1:step),1:step,r(1:step));

%f2
figure;subplot(2,1,1),plot(1:step,y(1:step),1:step,r(1:step));
xlabel('step');legend('$y$','$r$');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(get(gca,'YLabel'),'Interpreter','latex');
axis([30 50 -15 15]);

subplot(2,1,2),plot(1:step,y(1:step),1:step,r(1:step));
xlabel('step');legend('$y$','$r$');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(get(gca,'YLabel'),'Interpreter','latex');
axis([step-30 step-20 -5.5 5.5]);

%fff
figure;subplot(2,1,1),plot(delta_Huz_norm);
xlabel('step');%legend('$y$','$r$');
ylabel('$\parallel H_{u\bar z} ^* - H_{u\bar z} \parallel $');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(get(gca,'YLabel'),'Interpreter','latex');


subplot(2,1,2),plot(delta_Huu_norm);
xlabel('step');%legend('$y$','$r$');
ylabel('$\parallel H_{uu} ^* - H_{uu} \parallel $');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(get(gca,'YLabel'),'Interpreter','latex');


%f3
figure;plot(delta_norm);
xlabel('step');ylabel('$\parallel \bar K ^* - \bar K \parallel $');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(get(gca,'YLabel'),'Interpreter','latex');


%f4
figure;plot(1:step,Huz_learn(:,:),1:step,Huu_learn(:));
xlabel('step');legend('$H_{u \bar z1}$','$H_{u \bar z2}$','$H_{u \bar z3}$',...
    '$H_{u \bar z4}$','$H_{u \bar z5}$','$H_{u \bar z6}$','$H_{uu}$');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(get(gca,'YLabel'),'Interpreter','latex');

%f5
figure;plot(e);
xlabel('step');legend('$e$');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(get(gca,'YLabel'),'Interpreter','latex');

figure;plot(dropout);
xlabel('step');%legend('$e$');
set(findall(gcf,'type','line'),'linewidth',2);
set(get(gca,'XLabel'),'FontSize',16);set(get(gca,'YLabel'),'FontSize',16);
set(gca,'fontsize',16);set(legend,'Interpreter','latex');
set(get(gca,'YLabel'),'Interpreter','latex');

save('C:\\Users\\YiJia\\Desktop\\ADP2\\a3K_bar.mat','K_bar') 
