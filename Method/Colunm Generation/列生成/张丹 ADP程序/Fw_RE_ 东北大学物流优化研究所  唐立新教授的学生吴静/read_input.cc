// 
// C++ code for reading input files lp.txt
//
// Dan Zhang

// The data in the file lp.txt is in the following order
// T -- Time periods T (1*1)
// N -- Number of products N (1*1)
// M -- Number of resources M (1*1)
// L -- Number of segments L (1*1)
// c -- Capacity vector (1*M)
// lambda -- Arrival rate for each segment (1*L)
// a^T -- Resource consumption matrix a transposed (N*M)
// b^T -- Segment coincidence matrix b transposed (N*L)
// f -- Fare (N*1)
// MNLv -- MNL value for each product (N*1)
// MNLv0 -- MNL value for no-purchase option in each segment (L*1)


#include "network.h"

// #define IN_READ_INPUT

using namespace std;
using namespace Input;

// define namespace Input
int Input::T;    		      
int Input::N;    			
int Input::M;    		
int Input::L;	
vector<int> Input::c;   	  
vector<double> Input::lambda;  
vector< vector<int> > Input::a;
vector< vector<int> > Input::b;
vector<double> Input::f; 
vector<double> Input::MNLv;
vector<double> Input::MNLv0;

int Input::read_input()

// read input file data
// return 0 if everything read correctly, 1 otherwise.

{
  ifstream infile("lp.txt");

  infile >> T >> N >> M >> L;
  c.resize(M+1);
  lambda.resize(L+1);
  a.resize(M+1);
  b.resize(L+1);

  int i,j;

  for(i=1;i<=M;i++)
    infile >> c[i];

  for(i=1;i<=L;i++) infile >> lambda[i];

  //*********************
  // Cut input in half
  //  for(i=1;i<=L;i++)
  //  lambda[i]=lambda[i]/2;
  //*********************


  for(i=1;i<=M;i++)
    {
      a[i].resize(N+1);
    }

  for(j=1;j<=N;j++)
    {
      for(i=1;i<=M;i++) infile >> a[i][j];
    }

  for(i=1;i<=L;i++)
    b[i].resize(N+1);

  for(j=1;j<=N;j++)
    {
      for(i=1;i<=L;i++)
	{
	  infile >> b[i][j];
	}
    }


  f.resize(N+1);
  for(j=1;j<=N;j++)
    {
      infile >> f[j];
    }

  MNLv.resize(N+1);
  for(j=1;j<=N;j++)
    {
      infile >> MNLv[j];
    }

  MNLv0.resize(L+1);
  for(j=1;j<=L;j++)
    {
      infile >> MNLv0[j];
    }  

  // Output input data
  cout << "T = " << T << endl;
  cout << "N = " << N << endl;
  cout << "M = " << M << endl;
  cout << "L = " << L << endl;
  
  for(i=1;i<=M;i++)
    cout << "c["<< i << "] = "<< c[i] << endl;
//   for(i=1;i<=L;i++)
//     cout << "lambda["<< i << "] = "<< lambda[i] << endl;
//   for(i=1;i<=M;i++)
//     for(j=1;j<=N;j++)
//       cout << "a[" << i << "][" << j << "] = " << a[i][j] << endl;
//   for(i=1;i<=L;i++)
//     for(j=1;j<=N;j++)
//       cout << "b[" << i << "][" << j << "] = " << b[i][j] << endl;

//   for(j=1;j<=N;j++)
//     cout << "f[" << j << "] = " << f[j] << endl;
//   for(j=1;j<=N;j++)
//     cout << "MNLv[" << j << "] = " << MNLv[j] << endl;
//   for(j=1;j<=L;j++)
//     cout << "MNLv0[" << j << "] = " << MNLv0[j] << endl;
  return 0;

}




