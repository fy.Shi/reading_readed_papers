// Header file for C++ implementation of network RM with choice
//
// Dan Zhang, 12/20/05
//

#include <vector>
#include <numeric>
#include <ilcplex/ilocplex.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <time.h>
#include <ctime>
#include <iomanip>
#include <sys/times.h>
#include <float.h>

#define EPS .0001

using namespace std;

// application options -- to be done


// input data
namespace Input{
  extern int T; // time horizon
  extern int N; // number of products (classes)
  extern int M; // number of resources
  extern int L; // number of segments
  extern vector<int> c; // initial capacities
  extern vector<double> lambda; // arrival rate
  extern vector< vector<int> > a; // resource-class consumption matrix
  extern vector< vector<int> > b; // segment-class incidence matrix
  extern vector<double> f; // fare for each class
  extern vector<double> MNLv; // value used in MNL for each class
  extern vector<double> MNLv0; // no-purchase value for each segment 

  extern int read_input();
};


// Construct solution
namespace Solve {

  // decision variables for subproblem 
  extern IloIntVarArray r; // x[i] = current capacity of resource i
  extern IloNumVarArray y; // z variable in the reformulated subproblem
  extern IloNumVarArray alpha; // alpha is used in the change of variables

  class Column {
  public:
    int t;
    vector<IloNum> r;
    vector<IloNum> u;

    double xobj;
    vector<double> xcoef;   // xcoeff[i] for constraint flow[t,i]

    IloNumVar X_var;
    double xval;

    int non_usage_count;  // number of times X > 0

    ~Column()
      {
	X_var.end();
      }

    Column()
      {  r.resize(Input::M+1); u.resize(Input::N+1); 
      xcoef.resize(Input::M+1); 
      non_usage_count = 0;
      }
    Column(IloEnv env)
      {  r.resize(Input::M+1); u.resize(Input::N+1); 
      xcoef.resize(Input::M+1); 
      X_var = IloNumVar(env);
      non_usage_count = 0;
      }
    Column(int t2, vector<IloNum> r2, vector<IloNum> u2)
      {
	t = t2;
	r= r2;
	u = u2;
	
	xcoef.resize(Input::M+1); 
	compute_data();
	non_usage_count = 0;

      }


    void compute_data()
      {
	// compute xcoef
	// assume xcoef already sized
	int i,j,l;
	vector <double> sumv; // variable used for intermediate calculation
	vector <double> sumv1;
	vector <double> sumv2;
	sumv.resize(Input::L+1);
	sumv1.resize(Input::L+1);
	sumv2.resize(Input::L+1);

	xobj=0;
	for(l=1;l<=Input::L;l++)
	  {
	    sumv1[l]=0;
	    sumv2[l]=0;
	    for(j=1;j<=Input::N;j++)
	      {
		sumv1[l] += Input::MNLv[j] * u[j] * Input::b[l][j];
		sumv2[l] += u[j]*Input::MNLv[j]*Input::f[j]*Input::b[l][j];
	      }
	    xobj += Input::lambda[l]*sumv2[l]/(sumv1[l]+Input::MNLv0[l]);
	  
	  }



	for(i=1;i<=Input::M;i++)
	  {
	    xcoef[i] = r[i];

	    // part of xcoef[i] calculation
	    for(l=1;l<=Input::L;l++)
	      {
		sumv[l]=0;

		for(j=1;j<=Input::N;j++)
		  {
		    sumv[l] += Input::a[i][j] * Input::MNLv[j] * u[j] * Input::b[l][j];
		  }
		xcoef[i] -= Input::lambda[l]*sumv[l]/( sumv1[l] + Input::MNLv0[l] );
		
	      }
 
	    xcoef[i] *= -1;
	  }
      }
  };


    // master problem
    extern IloEnv env;
    extern IloModel master;
    extern IloCplex master_cplex;
    extern vector< vector< Column*> > column;
    extern IloObjective obj;

    extern vector<vector<IloRange> > flow;
    extern vector<IloRange>  sum_to_one;

    extern vector<vector<IloNum> > V;  // dual price on flow
    extern vector<IloNum> theta;           // dual price on sum_to_one

    // subproblem
    extern IloModel subproblem;
    extern IloCplex subproblem_cplex;
    extern vector< vector<IloRange> > resource_use;  // constraints
    extern vector<IloRange> change_of_variable;
    extern vector<IloRange> y_binary;
    extern IloObjective sub_obj;
    extern vector<IloNum> r_val;
    extern vector<IloNum> u_val;

    // functions
    extern void add_column(Column*);
    extern int init_subproblem();
    extern void update_obj(int);
    extern void get_prices();
    extern void get_subsolution(IloCplex&);
    extern double rp(int);
    extern void r_constraint(int);
    extern void Solve::r_constraint(int t, vector<int>& state);
    extern double rp(int, vector<int>&);
    extern double rp(int, vector<IloNum>&, vector<IloNum>&);
    extern void r_constraint(int, vector<int>&);
    //extern IloCplex::Callback collect_soln(Solutions&);
    //extern Solutions solutions;
    extern void scroll_time(bool, vector<int>&);
    extern int resolve_lp();
    extern bool heuristic(int, vector<int>);

    extern void Solve::scroll_time(bool state_change, vector<int>& state);

    extern bool price_out(int&,int);
    extern bool price_out(int&,int,vector<int>&);
    extern IloNum compute_new_r(int, vector<IloNum>&);
    extern vector<IloNumVarArray> Z;
    extern IloNumVarArray mu;
    extern void delete_column(Column*);

    extern int init_dp_subproblem();
    extern void Solve::update_dp_subobj(int t,vector<double>& next_value,vector<double>& valuediff);
    extern void fix_r_constraint(int,int);
};

namespace VL_Solve {
  extern IloNumVarArray VL_u;

  class VL_Column{
  public:
    vector<IloNum> VL_u;

    double RS;
    vector<double> QS;

    IloNumVar t_var; // decision variable
    double tvar;

    ~VL_Column()
      {
	t_var.end();
      }

    VL_Column()
      {
	VL_u.resize(Input::N+1);
	QS.resize(Input::M+1);
      }

    VL_Column(IloEnv env)
      {
	VL_u.resize(Input::N+1);
	t_var = IloNumVar(env);
	QS.resize(Input::M+1);
      }

    VL_Column(vector<IloNum> u2)
      {
	VL_u = u2;
	QS.resize(Input::M+1);
	VL_compute_data();
      }

    void VL_compute_data()
      {
	int i,j,l;
	vector <double> sumv; // variable used for intermediate calculation
	vector <double> sumv1;
	vector <double> sumv2;
	sumv.resize(Input::L+1);
	sumv1.resize(Input::L+1);
	sumv2.resize(Input::L+1);

	RS=0;
	for(l=1;l<=Input::L;l++)
	  {
	    sumv1[l]=0;
	    sumv2[l]=0;
	    for(j=1;j<=Input::N;j++)
	      {
		sumv1[l] += Input::MNLv[j] * VL_u[j] * Input::b[l][j];
		sumv2[l] += VL_u[j]*Input::MNLv[j]*Input::f[j]*Input::b[l][j];
	      }
	    RS += Input::lambda[l]*sumv2[l]/(sumv1[l]+Input::MNLv0[l]);
	  
	  }



	for(i=1;i<=Input::M;i++)
	  {
	    QS[i]=0;

	    for(l=1;l<=Input::L;l++)
	      {
		sumv[l]=0;

		for(j=1;j<=Input::N;j++)
		  {
		    sumv[l] += Input::a[i][j] * Input::MNLv[j] * VL_u[j] * Input::b[l][j];
		  }
		QS[i] += Input::lambda[l]*sumv[l]/( sumv1[l] + Input::MNLv0[l] );
		
	      }
	  }


#ifdef DEBUG_vlsolve
	for(j=1;j<=Input::N;j++)
	  cout << "u[" << j << "] = " << VL_u[j] << endl;

	for(i=1;i<=Input::M;i++)
	    cout << "QS["<< i << "] = " << QS[i] << endl;
	cout << "RS = " << RS << endl;
#endif

      }
  };

    // master problem
    extern IloEnv env;
    extern IloModel vl_master;
    extern IloCplex vl_master_cplex;
    extern vector< VL_Column* > vl_column;
    extern IloObjective vl_obj;

    extern vector<IloRange> resource;
    extern IloRange total_time;

    extern vector<IloNum> pi;
    extern IloNum sigma;

    extern vector<int> u_val;

    extern void VL_add_column(VL_Column* column);
    extern vector<int> VL_solve_sub(vector<double> pi);
    extern double VL_sub_value(vector<int> u, vector<double> pi,double sigma);

};


namespace Timer {
  class Time_lapse {
  public:
    time_t start_time;
    time_t stop_time;
    double elapsed;

    Time_lapse()
      {
	start_time = time(0);
      }
    void stop() {
      stop_time = time(0);
      elapsed = difftime(stop_time, start_time);
    }
    ~Time_lapse()
      {
	stop();
	cout << "Elapsed (processor) time = " << setw(10) <<
	  setprecision(10) << elapsed << " seconds" << endl;
	cerr << "Elapsed (processor) time = " << setw(10) <<
	  setprecision(10) << elapsed << " seconds" << endl;
      }
  };

  class Time_clock {
  public:
    struct tms *cputimes;
    int timeerror;
    double
      usertime,             /* CPU time in user mode */
      systime,               /* CPU time in system mode */
      cumulative;           /* cumulative CPU time in user mode */

    Time_clock()
      {
	cputimes = (struct tms *) malloc((unsigned) sizeof(struct tms));
	cumulative = 0;
	start(); 
      }

    void start() {
      timeerror = times(cputimes);
      if (timeerror == -1)
	{
	  fprintf(stderr, "\nError in resource usage function\n");
	}
      else
	{
	  usertime = (double) cputimes->tms_utime / sysconf(_SC_CLK_TCK);
	  systime = (double) cputimes->tms_stime / sysconf(_SC_CLK_TCK);
	}
    }

    void stop() {
      timeerror = times(cputimes);
      if (timeerror == -1)
	{
	  fprintf(stderr, "\nError in resource usage function\n");
	}
      else
	{
	  usertime = (double) cputimes->tms_utime / sysconf(_SC_CLK_TCK) - usertime;
	  systime = (double) cputimes->tms_stime / sysconf(_SC_CLK_TCK) - systime;
	  cumulative += usertime;
	}
    }
    double reset()
      {
	double temp;
	cumulative = 0;
	stop();
	temp = usertime;
	start();
	return temp;
      }
    ~Time_clock()
      {
	stop();
	//cout << "Elapsed (processor) time = " << setw(10) <<
	//     setprecision(10) << elapsed << " seconds" << endl;
	//cerr << "Elapsed (processor) time = " << setw(10) <<
	//     setprecision(10) << elapsed << " seconds" << endl;
      }
  };
};


namespace Generate {

  class Uniform {
  public:
    double Next()
      {
	return  ((double) rand()/((double) RAND_MAX+1.0));
      }
  };

 
  class random_seed {
  public:
    unsigned int seed;
    Uniform uniform;
    random_seed()
      {
	ifstream infile("seed.dat");
	if (!infile)
	  seed = (unsigned)time( NULL ) ;  // default seed
	else
	  infile >> seed;

	srand(seed);
	infile.close();

      }
    ~random_seed()
      {
	/*             if(Options::keep_seed == false) */
	/* 	    { */
	//ofstream outfile("seed.dat");
	//outfile << (unsigned)time( NULL ) << endl;
	//outfile << (unsigned) (time( NULL )*uniform.Next()) << endl;
	//outfile.close();
	/* 	    } */
      }

    void reseed()
      {
	srand(seed);
      }
  };

  class Discrete_Uniform {
  public:
    int Next(int a, int b)
      {
	// DU(a,b) inclusive
	static Uniform uniform;
	int r;
	r = int( (b-a+1)*uniform.Next() + a) ;

	return (r==b+1) ? b : r;
      }
  };

  class Exp {
  public:
    double lambda;
    Exp(double Lambda)
      {
	lambda = Lambda;
      }
    double Next()
      {
	static Uniform uniform;
	return ((-log(1-uniform.Next())/lambda));
      }
  };

  class Arrival {
  public:
    int Next()
      {
	static Uniform uniform;
	double sample = uniform.Next();
	int l;
	double temp = 0;
	for(l=1;l<=Input::L;l++)
	  {
	    temp += Input::lambda[l];
	    if (sample < temp)
	      break;
	  }
	if (l > Input::L) l = 0;

	return l;
      }

    double RandChoice()
      {
	static Uniform uniform;
	return uniform.Next();
      }
  };
};

namespace Simulate {
  extern vector<int> current_state;

  extern bool class_feasible(int&, vector<int>&);
  extern bool consume(int&, vector<int>&);
  extern bool price_out(int&);

  extern vector<int> ranking(vector<double>& current_value,vector<int>& current_state);
  extern vector<double> MNLchoice(vector<int>& usim);
  extern int Choice(vector<double>& prob, int& arrv, double& randchoice);

};

extern int cgsolve();
extern int cgresolve();
extern void sim_network();
extern int sim(bool,char*,int);
extern int vlsolve();
extern vector< vector<double> > decompose( vector< vector<double> >& value, int& k, int& cap, int& periods);
extern vector< vector<double> > decompose2( vector< vector<double> >& value, int& k, int& cap, int& periods);
extern int simVL(bool,char*,int);

// variables used in simulation
extern int sim_times;
//extern int resolve_times;
//extern int VL_resolve_times;

  


