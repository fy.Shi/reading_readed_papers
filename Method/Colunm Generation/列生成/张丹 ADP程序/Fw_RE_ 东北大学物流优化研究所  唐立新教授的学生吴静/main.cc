//
// C++ code for network RM with customer choice
// Dan Zhang, 12/20/05
//
             
#include "network.h"
       
// IloEnv Solve::env; 
         
using namespace std;
Timer::Time_clock timer;
vector<int> Simulate::current_state;

int sim_times=10;

int main()
{
  Timer::Time_lapse global_timer;
  double obj_value;

  cout << endl << "Network RM with Customer Choice" << endl;
  
  // read input data
  if(Input::read_input())
    {
      cerr << endl << "ERROR: Reading input" << endl;
      exit(1);
    }
   
  // solve
//     if(cgsolve())
//    {
//      cerr<<endl<<"ERROR: Solving LP"<<endl;
//    }

  // simulate  

    ofstream outfile("seed.dat");
    outfile << (unsigned)time( NULL ) << endl;
    outfile.close();

    char* filename;

    vector<int> original_c;
    original_c.resize(Input::M+1);

    original_c=Input::c;
    int original_T=Input::T;
  

 //    // ***********solve 1 time*******
    filename="VL_stats_reward_dc";
    simVL(1,filename,1);
    
    Input::c=original_c;
    Input::T=original_T; 

    filename="VL_stats_reward";
    simVL(0,filename,1);


//     //***********solve 5 time*******
//     Input::c=original_c;
//     Input::T=original_T; 

//     filename="VL_stats_reward_dc_5";
//     simVL(1,filename,5);
    
//     Input::c=original_c;
//     Input::T=original_T; 

//     filename="VL_stats_reward_5";
//     simVL(0,filename,5);


//     // ***********solve 10 time*******
//     Input::c=original_c;
//     Input::T=original_T; 

//     filename="VL_stats_reward_dc_10";
//     simVL(1,filename,10);
    
//     Input::c=original_c;
//     Input::T=original_T; 

//     filename="VL_stats_reward_10";
//     simVL(0,filename,10);

//     //********ADP from functional approximation************
    Input::c=original_c;
    Input::T=original_T;
    
    filename="stats_reward_dc";
    sim(1,filename,1);

    Input::c=original_c;
    Input::T=original_T;

    filename="stats_reward";
    sim(0,filename,1);

//     //********ADP from functional approximation 5 times************
//     Input::c=original_c;
//     Input::T=original_T;
    
//     filename="stats_reward_dc_5";
//     sim(1,filename,5);

//     Input::c=original_c;
//     Input::T=original_T;

//     filename="stats_reward_5";
//     sim(0,filename,5);

//     //********ADP from functional approximation 10 times************
//     Input::c=original_c;
//     Input::T=original_T;
    
//     filename="stats_reward_dc_10";
//     sim(1,filename,10);

//     Input::c=original_c;
//     Input::T=original_T;

//     filename="stats_reward_10";
//     sim(0,filename,10);
  
// output results

}











