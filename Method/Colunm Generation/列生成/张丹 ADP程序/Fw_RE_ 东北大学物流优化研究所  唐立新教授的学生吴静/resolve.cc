#include "network.h"

using namespace Input;
using namespace Solve;


// Formulate and solve the LP for bid price control
// Return:  u vector, where u[j] = 1 if accept j, 0 otherwise

vector<bool> solve_bpc(vector<int>& state, int& horizon, double& obj_value)
{
    try {
       IloModel bpc(env);


       // variables
       IloNumVarArray Y(env, N+1, 0, IloInfinity);

       int i,j;
       for(j=1;j<=N;j++)
       {
	       Y[j].setLB(0.0);
	       Y[j].setUB(horizon*bernoulli[j]);
       }

       // objective function
       IloObjective obj;
       obj = IloAdd(bpc, IloMaximize(env));
       for(j=1;j<=N;j++)
            obj.setCoef(Y[j], f[j]);

       // constraints 
       IloRangeArray capacity(env,M+1, 0, IloInfinity);

       for(i=1;i<=M;i++)
       {
	       capacity[i].setLB(0);
	       capacity[i].setUB(state[i]);
	       for(j=1;j<=N;j++)
		       capacity[i].setCoef(Y[j],a[i][j]);
       }
       bpc.add(capacity);

       IloCplex bpc_cplex(env);
       bpc_cplex.extract(bpc);
       //bpc_cplex.setParam(IloCplex::PreInd, 0); // turn off presolve
       //bpc_cplex.setParam(IloCplex::RootAlg, 2); 
       bpc_cplex.setParam(IloCplex::SimDisplay, 0);


       //cout << bpc;

       bpc_cplex.solve();
       //cout << "BPC optimal objective value = " << bpc_cplex.getObjValue() << endl;
       obj_value = bpc_cplex.getObjValue();

       vector<bool> u_bpc;
       u_bpc.resize(N+1);
       for(j=1;j<=N;j++)
       {
	       if (bpc_cplex.getReducedCost(Y[j]) > -EPS)
		       u_bpc[j] = true;
	       else
		       u_bpc[j] = false;
       }

       /*
       for(j=1;j<=N;j++)
       {
	       cout << "Y[" << j << "] = " << bpc_cplex.getValue(Y[j])
		        << "\t  u_bpc[" << j << "] = " << u_bpc[j]
			<< "\t  RC[" << j << "] = " 
			<< bpc_cplex.getReducedCost(Y[j])
			<< endl;
       }
       */

       bpc_cplex.end();
       bpc.end();

       return u_bpc;

    }
    catch (IloException& ex) {
      cerr << "Error: " << ex << endl;
    }
    catch (...) {
      cerr << "Error" << endl;
    }
}
