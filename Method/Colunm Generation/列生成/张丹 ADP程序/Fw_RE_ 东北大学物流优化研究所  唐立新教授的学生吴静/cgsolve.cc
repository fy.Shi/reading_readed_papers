// solve network RM with column generation

//#define DEBUG_solve_lp
//#define  DEBUG_resolve_lp

#include "network.h"

using namespace Input;
using namespace Solve;

// declarations in namespace Solve
// master
IloEnv Solve::env;
IloModel Solve::master;
IloCplex Solve::master_cplex;
vector< vector< Solve::Column*> > Solve::column;
IloObjective Solve::obj;
     
vector<vector<IloRange> > Solve::flow;
vector<IloRange> Solve::sum_to_one;

vector<IloNumVarArray> Solve::Z;
IloNumVarArray Solve::mu;

// subproblem
IloModel Solve::subproblem;
IloCplex Solve::subproblem_cplex;
IloIntVarArray Solve::r; // r[i] = current capacity of resource i
// IloNumVarArray Solve::u; // u[j] = 0,1 decision to accept/reject class j
IloNumVarArray Solve::y; // the variable z in the subproblem
IloNumVarArray Solve::alpha;
vector< vector<IloRange> > Solve::resource_use;  // constraints
vector<IloRange> Solve::change_of_variable;
vector<IloRange> Solve::y_binary; // y_j can only take value 0 or alpha_l for j\in N_l
IloObjective Solve::sub_obj;
vector<IloNum> Solve::r_val;
vector<IloNum> Solve::u_val;
vector<vector< IloNum> > Solve::V;
vector<IloNum> Solve::theta;


// Solve each subproblem to optimality
int cgsolve()
{  
  try {
    int i,j,l,t,t2;
	
    double old_obj_value = 0;
    int num_no_improve;

    double total_reduced_profit;
    double partial_reduced_profit;
    double temp;
    double current_obj_value;

    Timer::Time_clock master_timer;
    Timer::Time_clock subproblem_timer;
    master_timer.stop();
    subproblem_timer.stop();

    int num_deleted = 0;
    int num_added = 0;

    vector<Column*>::iterator it;
    int k;

    // definitions
    master = IloModel(env);
    master_cplex = IloCplex(env);
    column.resize(T+1);

    obj = IloAdd(master, IloMaximize(env));

    // define constraints 
    flow.resize(T+1);
    sum_to_one.resize(T+1);
    for(t=1;t<=T;t++)
      {
	if(t!=1)
	  sum_to_one[t] = IloRange(env,0,0);
	else
	  sum_to_one[1] = IloRange(env,1,1);
	master.add(sum_to_one[t]);

	flow[t].resize(M+1);
	for(i=1;i<=M;i++)
	  {
	    if (t!=1)
	      flow[t][i] = IloRange(env,0,0);
	    else
	      flow[1][i] = IloRange(env,c[i],c[i]);
	    master.add(flow[t][i]);
	  }
      }


    // add monotonicity
    mu = IloNumVarArray(env, T+1,0,IloInfinity); 
    Z.resize(T+1);
    for(t=1;t<=T;t++)
      Z[t] = IloNumVarArray(env, M+1,0, IloInfinity);

    for(t=1;t<=T;t++)
      {
	sum_to_one[t].setCoef(mu[t], 1);
	if (t>1)
	  sum_to_one[t].setCoef(mu[t-1],-1);

	for(i=1;i<=M;i++)
	  {
	    flow[t][i].setCoef(Z[t][i], 1);
	    if (t > 1)
	      flow[t][i].setCoef(Z[t-1][i], -1);
	  }
      }


    // reserve space for prices
    theta.resize(T+1);
    V.resize(T+2);
    for(t=1;t<=T+1;t++)
      V[t].resize(M+1);

    // initial columns
    Column *new_column;
    for(t=1;t<=T;t++)
      {
	new_column = new Column(env);
	new_column->t = t;
	for(i=1;i<=M;i++)
	  new_column->r[i] = c[i];
	for(j=1;j<=N;j++)
	  new_column->u[j] = 0;

	//	new_column->xobj = 0;
	//	for(i=1;i<=M;i++)
	//		new_column->xcoef[i] = c[i];

	new_column->compute_data();

	column[t].push_back(new_column);
	add_column(new_column);
	num_added++;
      }

    init_subproblem();

    // start with advanced solution
    //subproblem_cplex.setParam(IloCplex::MIPStart, 1);

    master_cplex.extract(master);
    master_cplex.setParam(IloCplex::SimDisplay, 0);
    master_cplex.setParam(IloCplex::PerInd, 1);
    //CPXsetintparam(master_cplex,CPX_PARAM_DATACHECK,CPX_ON);
    //master_cplex.setParam(IloCplex::EpRHS, .001);


    //   USE BARRIER
    master_cplex.setParam(IloCplex::RootAlg, CPX_ALG_BARRIER);
    master_cplex.setParam(IloCplex::BarCrossAlg, -1);
    master_cplex.setParam(IloCplex::BarDisplay, 0);
    
    //master_cplex.setParam(IloCplex::Threads, Options::threads);

    //master_cplex.setParam(IloCplex::EpPer, .0001);

	
    master_timer.start();
    master_cplex.solve();
    master_timer.stop();
 
    // main column generation loop
    //    double reduced_profit = 2*EPS; 
    while(true)
      {
// #ifdef DEBUG_solve_lp
// 	cout << "reduced_profit = " << reduced_profit << endl;
// #endif

	current_obj_value = master_cplex.getObjValue();

	// if improvement
	if (old_obj_value < current_obj_value + EPS)
	  {
	    old_obj_value = current_obj_value;
	    num_no_improve = 0;
	  }
	else
	  num_no_improve++;
             
#ifdef DEBUG_solve_lp
	//cout << master;
	cout << "obj = " << master_cplex.getObjValue() << endl;
#endif

	//outfile << master_cplex.getObjValue() << endl;
	get_prices();

	// optimize each subproblem backwards through time
	total_reduced_profit = 0;
	for(t=T;t>=1;t--)	   
	  {
	    update_obj(t);
	    r_constraint(t);

	    subproblem_timer.start();
	    if (subproblem_cplex.solve() != IloTrue)
	      throw "Can't solve subproblem";
	    subproblem_timer.stop();

	    for (i=1;i<=M;i++)
	      r_val[i] = subproblem_cplex.getValue(r[i]);
	    	   	    
	    double u_tmp;
	    for(j=1;j<=N;j++)
	      {
		u_tmp=0;
		for(l=1;l<=L;l++)
		  {
		    if(b[l][j]>0)
		      u_tmp = subproblem_cplex.getValue(y[j])/subproblem_cplex.getValue(alpha[l]);
		  }
		//cout << u_tmp << endl;
		u_val[j]=u_tmp;
		// u_val[j] = rint(u_tmp);
	      }

	    //#ifdef DEBUG_solve_lp
// 	    for(i=1;i<=M;i++)
// 	      cout << "r_val[" << i << "] = " << r_val[i] << endl;
 	    //for(j=1;j<=N;j++)
 	    //  cout << "u_val[" << j << "] = " << u_val[j] << endl;
	    //#endif

	    total_reduced_profit += subproblem_cplex.getObjValue();
// #ifdef DEBUG_solve_lp
// 	    cout << "subobj=" <<subproblem_cplex.getObjValue()<<endl; 
// #endif
	    // if positive reduced cost
	    if (subproblem_cplex.getObjValue() > EPS)
	      {
		// add column
		new_column = new Column(env);
		new_column->t = t;
		for(i=1;i<=M;i++)
		  new_column->r[i] = r_val[i];
		for(j=1;j<=N;j++)
		  new_column->u[j] = u_val[j];

		new_column->compute_data();

		column[t].push_back(new_column);
		add_column(new_column);
		num_added++;
// #ifdef DEBUG_solve_lp
// 		cout << "adding to period " << t << endl;
// #endif
	      }
  	  }

	master_timer.start();
	master_cplex.solve();
	master_timer.stop();

#ifdef DEBUG_solve_lp
		cout << "total reduced profit = " << total_reduced_profit << endl;
#endif


		// if satisfy optimality tolerance, then stop
				 if ((total_reduced_profit/master_cplex.getObjValue() < 0.005)
 		 || (total_reduced_profit < EPS && master_cplex.getObjValue() < EPS) )
				   //		   	if(total_reduced_profit <= EPS )
	  {
	    get_prices();
	    cout << "T = " << T << "     final obj = " << master_cplex.getObjValue() << endl;
	    cout << "num_deleted = " << num_deleted << endl;
	    cout << "num_added = " << num_added << endl;
	    cout << "Time solving master: " << master_timer.cumulative << " CPU seconds" <<  endl;
	    cout << "Time solving subproblems: " << subproblem_timer.cumulative << " CPU seconds" << endl;
	    return 0;

  	  }
      }
  }
  catch (IloException& ex) {
    cerr << "Error: " << ex << endl;
  }
  catch (...) {
    cerr << "Error" << endl;
  }


}

void Solve::add_column(Column* column)
{
  int i,t;
  t = column->t;

  // add column to master problem
  Solve::obj.setCoef(column->X_var, column->xobj);
 
  sum_to_one[column->t].setCoef(column->X_var, 1);
  if(t<T)
    sum_to_one[t+1].setCoef(column->X_var,-1);

//   cout << "obj " << column->xobj << endl;
//   for(i=1;i<=Input::M;i++)
//     cout << "col " << i << " " << column->xcoef[i] << endl;

  for(i=1;i<=M;i++)
    flow[t][i].setCoef(column->X_var, column->r[i]);
  if (t < T)
    for(i=1;i<=M;i++)
      flow[t+1][i].setCoef(column->X_var, column->xcoef[i]);
}


int Solve::init_subproblem()
{
  int i,j,l;

  subproblem = IloModel(env);
  subproblem_cplex = IloCplex(env);

  // variables
  r = IloIntVarArray(env, M+1);
  for(i=1;i<=M;i++)
    r[i] = IloIntVar(env,0,c[i]);
  y = IloNumVarArray(env,N+1);
  for(j=1;j<=N;j++) 
    y[j] = IloNumVar(env,0,IloInfinity);
  alpha =IloNumVarArray(env,L+1);
  for(l=1;l<=L;l++)
    alpha[l] = IloNumVar(env,0,IloInfinity);

  // constraints
  resource_use.resize(M+1);

  for(i=1;i<=M;i++)
    {
      resource_use[i].resize(N+1);

      for(j=1;j<=N;j++)
	{
	  IloExpr expr(env);
	  for(l=1;l<=L;l++)
	    expr += b[l][j]*a[i][j]*MNLv0[l]*y[j];
	  expr -= r[i];

	  resource_use[i][j] = IloRange(env,-IloInfinity,expr,0);
	  expr.end();
	}
    }

  // change_of_variable constraint
  change_of_variable.resize(L+1);

  for(l=1;l<=L;l++){

    IloExpr expr(env);

    for(j=1;j<=N;j++)
      expr += b[l][j]*MNLv[j]*y[j];
    expr += MNLv0[l]*alpha[l];

    change_of_variable[l] = IloRange(env,1,expr,1);
    expr.end();    
  }

  // y_binary constraints
  y_binary.resize(N+1);
  for(j=1;j<=N;j++)
    {
      IloExpr expr(env);
      for(l=1;l<=L;l++)
	expr -= b[l][j]*alpha[l];
      expr += y[j];
      y_binary[j] = IloRange(env,-IloInfinity,expr,0);
      expr.end();
    }

  // objective function
  sub_obj = IloMaximize(env);
  update_obj(T);

  // construct models
  subproblem.add(sub_obj);
  for(i=1;i<=M;i++)
    for(j=1;j<=N;j++)
      subproblem.add(resource_use[i][j]);

  for(l=1;l<=L;l++) subproblem.add(change_of_variable[l]);
  for(j=1;j<=N;j++) subproblem.add(y_binary[j]);

  subproblem_cplex.extract(subproblem);

  subproblem_cplex.setParam(IloCplex::MIPDisplay, 0);
  subproblem_cplex.setParam(IloCplex::SimDisplay, 0);
  subproblem_cplex.setParam(IloCplex::CutUp, EPS);
  //subproblem_cplex.setParam(IloCplex::Threads, Options::threads);
  //subproblem_cplex.setParam(IloCplex::IntSolLim, Options::num_int_sol);
  //subproblem_cplex.setParam(IloCplex::StrongThreadLim, Options::threads);

	
  //subproblem_cplex.setParam(IloCplex::PerInd, 1);
  //subproblem_cplex.setParam(IloCplex::EpPer, .01);
  //subproblem_cplex.setParam(IloCplex::IntSolLim, Options::num_int_sol);
	
  //subproblem_cplex.use(collect_soln(solutions));

  // make room for solution
  r_val.resize(M+1);
  u_val.resize(N+1);
}


void Solve::update_obj(int t)
  // Update coefficients in subproblem objective function
  // for time period t subproblem
{
  int i,j,l;
  double temp;

  for(j=1;j<=N;j++)
    {
      temp=0;
      for(l=1;l<=L;l++)
	{
	  temp += b[l][j]*lambda[l]*MNLv[j]*f[j];
	  if(t<T)
	    {
	      for(i=1;i<=M;i++)
		temp -= b[l][j]*lambda[l]*MNLv[j]*a[i][j]*V[t+1][i];
	    }
	}
      sub_obj.setCoef(y[j],temp);      
    }

  if(t<T)
    {
      for(i=1;i<=M;i++)
	sub_obj.setCoef(r[i], -(V[t][i]-V[t+1][i]));
  
      sub_obj.setConstant(-theta[t]+theta[t+1]);
    }
  else
    {
      for(i=1;i<=M;i++)
	sub_obj.setCoef(r[i], -V[t][i]);
  
      sub_obj.setConstant(-theta[t]);
    }
}


void Solve::get_prices()
{
  int i,t;
  for(t=1;t<=T;t++)
    {
      theta[t] = master_cplex.getDual(sum_to_one[t]);
      for(i=1;i<=M;i++)
	{
	  V[t][i] = master_cplex.getDual(flow[t][i]);
#ifdef DEBUG_solve_lp
	  cout << "V[" << t << "][" << i << "] = " << V[t][i] << endl;
#endif
	}
    }

#ifdef DEBUG_solve_lp
  for(t=1;t<=T;t++)
    cout << "theta[" << t << "] = " << theta[t] << endl;
#endif
}


void Solve::r_constraint(int t)
  // if t == 1, then fixed r[i]=c[i], otherwise r[i] >= 0
{
  int i;
  for(i=1;i<=M;i++)
    {
      if (t > 1)
	r[i].setLB(0);
      else
	r[i].setLB(c[i]);
    }
}

void Solve::r_constraint(int t, vector<int>& state)
  // if t == 1, then fixed r[i]=state[i], otherwise r[i] >= 0
{
  int i;
  for(i=1;i<=M;i++)
    {
      r[i].setUB(state[i]);

//       if (Options::truncate_state_space)
// 	{
// 	  r[i].setLB(max(0,state[i]-(t-1)));
// 	}
//       else
// 	{
	  if (t > 1)
	    r[i].setLB(0);
	  else
	    r[i].setLB(state[i]);
// 	}
    }
}


void Solve::scroll_time(bool state_change, vector<int>& state)
  // scroll time forward one time period, with initial state
{
  int i,j,t,n;

  // remove constraints from model
  for(i=1;i<=M;i++)
    master.remove(flow[T][i]);
  master.remove(sum_to_one[T]);


  // remove variables
  vector<Column*>::iterator it;
  for(it = column[T].begin(); it < column[T].end(); it++)
    {
      Solve::obj.setCoef((*it)->X_var, 0);
      master.remove( (*it)->X_var);
    }


  // zero out dual prices of removed constraints
  //for(i=1;i<=M;i++)
  //  V[T][i] = 0;
  // theta[T] = 0;
 
  T--;

  if (state_change)
    {	
      // print-out state
#ifdef DEBUG_resolve_lp
      for(i=1;i<=M;i++)
	cout << "new state[" << i << "] = " << state[i] << endl;
      cout.flush();
#endif

      // change rhs of time 1 flow constraints
      for(i=1;i<=M;i++)
	flow[1][i].setBounds(state[i],state[i]);

      // change RHS of subproblem constraints
      for(i=1;i<=M;i++)
	r[i].setUB(state[i]);


      // adjust existing columns
      for(t=1;t<=T;t++)
	for(it = column[t].begin(); it < column[t].end(); it++)
	  {

	    bool violate_resource=0;
	    for(i=1;i<=M;i++)
	      if( (*it)->r[i] > state[i] ) violate_resource=1;
	    if(violate_resource)
	      {
		(*it)->X_var.end();
		column[t].erase(it);		
	      }


// 	    for(i=1;i<=M;i++)
// 	      {
// 		(*it)->r[i] = min((*it)->r[i],(double) state[i]);

// 	      }

// 	    for(j=1;j<=N;j++)
// 	      {
// 		for(i=1;i<=M;i++)
// 		  if (a[i][j] > 0)
// 		    (*it)->u[j] = 
// 		      min( (*it)->u[j], floor((*it)->r[i]/a[i][j]));
// 	      }

// 	    (*it)->compute_data();
// 	    add_column((*it));
	  }


    }

  master_cplex.clear();
  master_cplex.extract(master);

}


int cgresolve()
{
  int i,j,l,t,t2;
  Column* new_column;
  // bool heuristic_flag;
  //	ofstream outfile("output_obj");

  double old_obj_value = 0;
  int num_no_improve;

  double total_reduced_profit;
  double partial_reduced_profit;
  double temp;
  double current_obj_value;

  int num_deleted = 0;
  int num_added = 0;
  int k;

  vector<Column*>::iterator it;

  try {
	
    master_cplex.solve();

    // main column generation loop
    double reduced_profit = 2*EPS; 
    while(true)
      {
#ifdef DEBUG_resolve_lp
	cout << "reduced_profit = " << reduced_profit << endl;
#endif

	current_obj_value = master_cplex.getObjValue();

	// if improvement
	if (old_obj_value < current_obj_value + EPS)
	  {
	    old_obj_value = current_obj_value;
	    num_no_improve = 0;
	  }
	else
	  num_no_improve++;
             
#ifdef DEBUG_resolve_lp
	//cout << master;
	cout << "obj = " << master_cplex.getObjValue() << endl;
#endif

	// outfile << master_cplex.getObjValue() << endl;
	get_prices();

#ifdef DEBUG_resolve_lp
	cout << "X Solution:" << endl;
	for(t=1;t<=T;t++)
	  {
	    k = 1;
	    for(it=column[t].begin();it<column[t].end();it++) 
	      {
		cout << "X[" << t << "][" << k << "] = " << 
		  master_cplex.getValue((*it)->X_var) << endl;
		k++;
	      }
	  }
#endif

// 	if (!Options::dont_delete_constraints)
//           {
// 	    // get X solution
// 	    for(t=1;t<=T;t++)
// 	      {
// 		for(it=column[t].begin();it<column[t].end();it++) 
// 		  (*it)->xval  =  master_cplex.getValue((*it)->X_var);
// 	      }

// 	    // delete columns that haven't been used for a long time
// 	    for(t=1;t<=T;t++)
// 	      {
// 		for(it=column[t].begin();it<column[t].end();it++) 
// 		  {
// 		    if ((*it)->xval < EPS)
// 		      {
// 			(*it)->non_usage_count++;
// 		      }
// 		    // reset
// 		    else
// 		      (*it)->non_usage_count = 0;

// 		    if ((*it)->non_usage_count > Options::max_non_usage)
// 		      {
// 			//obj.setCoef((*it)->X_var, 0);
// 			//master.remove( (*it)->X_var);

// 			// delete_column(*it);
// 			//(*it)->X_var.removeFromAll();
			       
// 			(*it)->X_var.end();
// 			column[t].erase(it);
// 			num_deleted++;
// 		      }
// 		  }
// 	      }
// 	    //cout << "num_deleted = " << num_deleted << endl;
// 	    //master_cplex.clear();
// 	    //master_cplex.extract(master);
// 	  }

	// optimize each subproblem backwards through time
	total_reduced_profit = 0;
	for(t=T;t>=1;t--)	   
	  {
	    update_obj(t);
	    r_constraint(t, Simulate::current_state);

	    if (subproblem_cplex.solve() != IloTrue)
	      throw "Can't solve subproblem";

	    for (i=1;i<=M;i++)
	      r_val[i] = subproblem_cplex.getValue(r[i]);

	    double u_tmp;
	    for(j=1;j<=N;j++)
	      {
		u_tmp=0;
		for(l=1;l<=L;l++)
		  {
		    if(b[l][j]>0)
		      u_tmp = subproblem_cplex.getValue(y[j])/subproblem_cplex.getValue(alpha[l]);
		  }
		//cout << u_tmp << endl;
		u_val[j]=u_tmp;
		// u_val[j] = rint(u_tmp);
	      }

	    //for(j=1;j<=N;j++)
	    //  u_val[j] = subproblem_cplex.getValue(u[j]);

#ifdef DEBUG_resolve_lp
	    for(i=1;i<=M;i++)
	      cout << "r_val[" << i << "] = " << r_val[i] << endl;
	    for(j=1;j<=N;j++)
	      cout << "u_val[" << j << "] = " << u_val[j] << endl;
	    cout << "solution's reduced profit = " << 
	      subproblem_cplex.getObjValue() << endl;
#endif

	    total_reduced_profit += subproblem_cplex.getObjValue();

	    // if positive reduced cost
	    if (subproblem_cplex.getObjValue() > EPS)
	      {

		// add column
		new_column = new Column(env);
		new_column->t = t;
		for(i=1;i<=M;i++)
		  new_column->r[i] = r_val[i];
		for(j=1;j<=N;j++)
		  new_column->u[j] = u_val[j];

		new_column->compute_data();

		column[t].push_back(new_column);
		add_column(new_column);
		num_added++;
#ifdef DEBUG_resolve_lp
		cout << "adding to period " << t << endl;
#endif
	      }
	  }

	if(master_cplex.solve())
	  {


	    if (IloIsNAN(master_cplex.getObjValue()))
	    {
	      for(i=1;i<=M;i++)
		{
		  cout << "c[" << i << "] = " << c[i] << endl;
		}
	      cout << "T = " << T << endl;

	    }
	
	    cout << "obj value = " << master_cplex.getObjValue() << endl;
	    cout << "total_reduced_prifit = " << total_reduced_profit << endl;
	    double ratio=total_reduced_profit/master_cplex.getObjValue();
	    cout << "master obj = " << master_cplex.getObjValue() << endl;
	    cout << "ratio = " << ratio << endl;

	    // if satisfy optimality tolerance, then stop
	     	    if ((total_reduced_profit/master_cplex.getObjValue() < 0.005)
 	    || (total_reduced_profit < EPS && master_cplex.getObjValue() < EPS) )
	    //	        	    if( total_reduced_profit<EPS ) 
	      {
		get_prices();
		//#ifdef DEBUG_resolve_lp
		cout << "T = " << T << "     final obj = " << master_cplex.getObjValue() << endl;
		cout << "num_deleted = " << num_deleted << endl;
		cout << "num_added = " << num_added << endl;
		//#endif
		return 0;

	      }
	  }
// 	else
// 	  {
// 	    // display DEBUG information and exit the program
// 	    for(i=1;i<=M;i++)
// 	      {
// 		cout << "c[" << i << "] = " << c[i] << endl;
// 	      }
// 	    cout << "T = " << T << endl;
// 	    exit;

// 	  }

      }
  }
  catch (IloException& ex) {
    cerr << "Error: " << ex << endl;
  }
  catch (...) {
    cerr << "Error" << endl;
  }
}




