======================================================================
shuthesis template update log
----------------------------------------------------------------------
20100605:
版本：shuthesis v1.5

更新：

1）亲笔签名页将“主任”变更为“主席”；

2）英文标题页面将
A Dissertation Submitted to Shanghai University for the 
Degree of Doctor in ××× 
变更为
A Dissertation Submitted to Shanghai University 
for the Degree of Doctor in ××× ；

3）中文标题页和封面使用统一的“作者”称呼；

4）目录和正文之间依次增添了插图索引、表格索引和符号及释义对照表章节页面；

5）将PDF文档的书签定位至页面内容处，避免纠结于所谓定位不准确的说法。

----------------------------------------------------------------------

20100602:
版本：shuthesis v1.4

更新：

1）对部分版面格式进行了微调，以期最大限度符合研究生部的论文规范。

2）增添了对模板使用的简介；

3）增添了对JabRef参考文献管理工具的简介。

----------------------------------------------------------------------

20100518：
版本：shuthesis v1.3

更新：

1）解决了在原创性声明页面显示页码的问题；

2）更换了封面上两张图片，取代以更高精度的图片；

3）取消了原空白页上“空白页”三个字；

4）演示了如何在TeX文档的个人信息录入项中改变字体的案例，
   详见shuthesis.tex文件中的\cauthor{}等处；

5）解决了脚注定位不正确的问题，注意SumatraPDF阅读器不能定位脚注的超链接，
   而Acrobat可以；命令\VerbatimFootnotes会导致所有脚注链接失效；

6）对部分版面格式进行了微调，以期最大限度符合研究生部的论文规范。

----------------------------------------------------------------------

20100512：
版本：shuthesis v1.2

更新：

1）解决了在 CTeX v2.8.0.125 下无法流畅编译的问题；

2）通过在TeX文档中的两个命令：shuname和shulogo，设置了上海大学名称
   和logo图片的路径和格式，方便使用者选择 LaTeX 或 PDFLaTeX 编译方式；

3）增大了脚注横线和文本之间的距离；

4）去掉了无用的 xjtu_mbib.sty 宏包；

5）增加了制作盲审论文的说明和样式文件；

6）试用成功，更改了版本编号命名规则。
----------------------------------------------------------------------