LaTeX and Word Templates for QoMEX 2017

Files:
- Qomex2017_paper.doc: Word template file

- Qomex2017_paper.tex: LaTeX template file
- Qomex2017_paper.pdf: PDF generated from the template file
- refs.bib: example file of bibliographic references
- IEEEtran.cls: LaTeX class file that defines how the document lokks like
- IEEEtran.bst: BiBTeX style file with bibliography style definitions


The manuscript template files can be also found in the following URL:

http://www.ieee.org/conferences_events/conferences/publishing/templates.html


